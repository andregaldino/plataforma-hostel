 $(".button-collapse").sideNav();

 $('select').material_select();

 $('.datepicker').pickadate({
 	labelMonthNext: 'Próximo mês',
 	labelMonthPrev: 'Mês anterior',
 	labelMonthSelect: 'Selecione um mês',
 	labelYearSelect: 'Selecione um ano',
 	monthsFull: [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro' ],
 	monthsShort: [ 'Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez' ],
 	weekdaysFull: [ 'Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado' ],
 	weekdaysShort: [ 'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab' ],
 	weekdaysLetter: [ 'D', 'S', 'T', 'Q', 'Q', 'S', 'S' ],
 	today: 'Hoje',
 	clear: 'Limpar',
 	close: 'Fechar',
 	format: 'dd/mm/yyyy',
	formatSubmit: 'dd/mm/yyyy',
 	onClose: function() {
 		$(document.activeElement).blur();
 	},
 	onSet: function(context) {
 		if ( 'select' in context ) {
 			this.close();
 		}
 		$('label[for="date"]').addClass('active');
 	}
 });