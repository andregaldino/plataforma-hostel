var options = {
	container: 'header',
	selectMonths: true,
	labelMonthNext: 'Próximo mês',
	labelMonthPrev: 'Mês anterior',
	labelMonthSelect: 'Selecione um mês',
	labelYearSelect: 'Selecione um ano',
	monthsFull: [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro' ],
	monthsShort: [ 'Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez' ],
	weekdaysFull: [ 'Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado' ],
	weekdaysShort: [ 'Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab' ],
	weekdaysLetter: [ 'D', 'S', 'T', 'Q', 'Q', 'S', 'S' ],
	today: 'Hoje',
	clear: 'Limpar',
	close: 'Fechar',
	format: 'dd/mm/yyyy',
	formatSubmit: 'yyyy-mm-dd',
	closeOnSelect: true,
	onStart: function() {
		$('.picker__weekday-display').hide();
		$('.picker__date-display').hide();
	},
	onRender: function() {
	},
	onOpen: function() {
		$('.picker__weekday-display').hide();
		$('.picker__date-display').hide();

	},
	onClose: function() {
		$(document.activeElement).blur();
	},
	onStop: function() {
	},
	onSet: function(context) {
		$('.picker__weekday-display').hide();
		$('.picker__date-display').hide();
		if ( 'select' in context ) {
			this.close();
		}
	},
	min: true
}

var from_$input = $('#check_in').pickadate(options);
var to_$input = $('#check_out').pickadate(options);

from_picker = from_$input.pickadate('picker');
to_picker = to_$input.pickadate('picker')

if ( from_picker.get('value') ) {
	to_picker.set('min', from_picker.get('select'))
}
if ( to_picker.get('value') ) {
	from_picker.set('max', to_picker.get('select'))
}

from_picker.on('set', function(event) {
	if ( event.select ) {
		to_picker.set('min', from_picker.get('select'))    
	}
	else if ( 'clear' in event ) {
		to_picker.set('min', false)
	}
})

to_picker.on('set', function(event) {
	if ( event.select ) {
		from_picker.set('max', to_picker.get('select'))
	}
	else if ( 'clear' in event ) {
		from_picker.set('max', false)
	}
})

$(document).ready(function(){
	$('.parallax').parallax();
});

$(window).scroll(function() {
	var nav = $('.navbar-menu');
	var distance = $(nav).offset().top;
	var range = 400;
	var scroll = $(window).scrollTop();
	var calc = 0 + scroll/range;
	nav.css('background', 'rgba(33, 33, 33, '+calc+')');
});

$( "#check" ).submit(function(e) {
	e.preventDefault();
	var check_in = $('input[name=check_in_submit]').val();
	var check_out = $('input[name=check_out_submit]').val();
	if (check_in && check_out) {
		window.location.href = "/booking?start_date=" + check_in + "&end_date=" + check_out
	}
	else {
		window.location.href = "/booking";
	}
});