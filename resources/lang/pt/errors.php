<?php 
return [
	'gallery' => [
		'noimage' => 'Nenhuma imagem encontrada',
		'nogallery' => 'Nenhum album encontrado',
	],
	'pagenotfound' => 'A pagina que você está procurando não foi encontrada.',
	'internal' => 'Alguma coisa está errada. Uma equipe de cangurus especialistas altamente treinados foi designada para cuidar do problema.'
];