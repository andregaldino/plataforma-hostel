<?php 
return [
	'booknow' => 'Reservar',
	'rooms' => [
		'rooms' => 'Quartos',
	],
	'photogallery' => 'Galeria de fotos',
	'event' => 'Eventos',
	'location' => 'Localização',
	'contact' => 'Contato',
	'news' => 'Novidades',
	'restrict' =>'Acesso restrito',
	'terms' =>'Termos e Condições',
	'blog' => 'Atrações',
	'booking' => 'Reservas',
	'404'	=>	'Página não encontrada',
	'reset' =>  'Alteração de senha',
];