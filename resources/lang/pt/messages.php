<?php 

return [
    'welcome' => 'Bem vindo ao nosso Hostel',
    'events' => 'Eventos',
    'about' => [
    	'about' => 'Sobre',
    	'message' => 'Nossa missão é melhorar constantemente o nível do nosso serviço para sempre ser capaz de oferecer um ambiente acolhedor e confortável em que os nossos hospedes possam se sentir mais à vontade do que em suas próprias casas.'
    ],
    'location' => 'Localização',
    'contactus' => 'Entre em contato',
    'connect' => 'Connect with us',
    'followus' => 'Siga-nos',
    'address' => 'Endereço',
    'links' => 'Links',
    'chooselang' => 'Escolha seu idioma',
    'categories' => 'Categorias',
    'read' => 'Leia mais',
    'developedby' => 'Desenvolvido por',
    'terms' => 'Termos e condições',
    'account'   =>  'Email de acesso',
    'reset' =>  'Alteração de senha',
    'thanks'    =>  'Obrigado pela mensagem, em breve entraremos em contato'
];