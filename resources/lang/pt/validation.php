<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | O following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'O :attribute deve ser aceitado.',
    'active_url'           => 'O :attribute não é uma URL valida.',
    'after'                => 'O :attribute deve ser uma data posterior a :date.',
    'alpha'                => 'O :attribute apenas pode conter letras.',
    'alpha_dash'           => 'O :attribute apenas pode conter letras, numeros, e caracteres especiais.',
    'alpha_num'            => 'O :attribute apenas pode conter letras e numeros.',
    'array'                => 'O :attribute deve ser um vetor.',
    'before'               => 'O :attribute deve ser uma data anterior a :date.',
    'between'              => [
        'numeric' => 'O :attribute deve ser entre :min e :max.',
        'file'    => 'O :attribute deve ser entre :min e :max kilobytes.',
        'string'  => 'O :attribute deve ser entre :min e :max characters.',
        'array'   => 'O :attribute deve ter entre :min e :max items.',
    ],
    'boolean'              => 'O campo :attribute  deve ser verdadeiro ou falso.',
    'confirmed'            => 'O :attribute confirmação não é equivalente.',
    'date'                 => 'O :attribute não é uma valido date.',
    'date_format'          => 'O :attribute não é equivalente ao formato :format.',
    'different'            => 'O :attribute e :other deve ser different.',
    'digits'               => 'O :attribute deve ser :digits digitos.',
    'digits_between'       => 'O :attribute deve ser entre :min e :max digitos.',
    'dimensions'           => 'O :attribute tem dimensões de imagem invalidas;',
    'distinct'             => 'O campo :attribute  tem um valor duplicado.',
    'email'                => 'O :attribute deve ser um email valido.',
    'exists'               => 'O :attribute selecionado é invalido.',
    'file'                 => 'O :attribute deve ser um arquivo.',
    'filled'               => 'O campo :attribute  é obrigatório.',
    'image'                => 'O :attribute deve ser um image.',
    'in'                   => 'O :attribute selecionado é invalido.',
    'in_array'             => 'O campo :attribute não existe em :other.',
    'integer'              => 'O :attribute deve ser um integer.',
    'ip'                   => 'O :attribute deve ser um endereço IP valido.',
    'json'                 => 'O :attribute deve ser uma string JSON valido.',
    'max'                  => [
        'numeric' => 'O :attribute não pode ser maior do que :max.',
        'file'    => 'O :attribute não pode ser maior do que :max kilobytes.',
        'string'  => 'O :attribute não pode ser maior do que :max characters.',
        'array'   => 'O :attribute não pode ter mais do que :max itens.',
    ],
    'mimes'                => 'O :attribute deve ser um arquivo do tipo: :values.',
    'mimetypes'            => 'O :attribute deve ser um arquivo do tipo: :values.',
    'min'                  => [
        'numeric' => 'O :attribute deve ser menor que :min.',
        'file'    => 'O :attribute deve ser menor que :min kilobytes.',
        'string'  => 'O :attribute deve ser menor que :min characters.',
        'array'   => 'O :attribute deve ter menor que :min itens.',
    ],
    'not_in'               => 'O :attribute selecionado é invalido.',
    'numeric'              => 'O :attribute deve ser um numero.',
    'present'              => 'O campo :attribute  deve estar presente.',
    'regex'                => 'O :attribute tem formato invalido.',
    'required'             => 'O campo :attribute  é obrigatório.',
    'required_if'          => 'O campo :attribute  é obrigatório quando o :other é :value.',
    'required_unless'      => 'O campo :attribute  é obrigatório unless :other é in :values.',
    'required_with'        => 'O campo :attribute  é obrigatório quando o :values é presente.',
    'required_with_all'    => 'O campo :attribute  é obrigatório quando o :values é presente.',
    'required_without'     => 'O campo :attribute  é obrigatório quando o :values é not presente.',
    'required_without_all' => 'O campo :attribute  é obrigatório quando nenhum dos :values estão presente.',
    'same'                 => 'O :attribute e :other deve ser equivalente.',
    'size'                 => [
        'numeric' => 'O :attribute deve ser :size.',
        'file'    => 'O :attribute deve ser :size kilobytes.',
        'string'  => 'O :attribute deve ser :size characters.',
        'array'   => 'O :attribute must contain :size items.',
    ],
    'string'               => 'O :attribute deve ser uma string.',
    'timezone'             => 'O :attribute deve ser uma zona valida',
    'unique'               => 'O :attribute ja esta sendo utilizado.',
    'uploaded'             => 'O :attribute falha para fazer upload.',
    'url'                  => 'O :attribute tem formato invalido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | O following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
