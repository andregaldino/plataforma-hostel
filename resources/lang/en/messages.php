<?php 

return [
    'welcome' => 'Welcome to our Hostel',
    'events' => 'Events',
    'about' => [
    	'about' => 'About',
    	'message' => 'Our mission is to be constantly improving the level of our service to always be able to offer a warm and comfortable environment in which our guests can feel more at ease than in their own homes.'
    ],
    'location' => 'Location',
    'address' => 'Address',
    'contactus' => 'Contact us',
    'connect' => 'Connect with us',
    'followus' => 'Follow us',
    'links' => 'Links',
    'chooselang' => 'Choose your language',
    'categories' => 'Categories',
    'read' => 'Read more',
    'developedby' => 'Developed by',
    'terms' => 'Terms and conditions',
    'account'   =>  'Email address',
    'reset' =>  'Reset password',
    'thanks'    =>  'Thank you for the message, soon we will be in touch'
];
