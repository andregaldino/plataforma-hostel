<?php 

return [
	'detail' => 'Room detail',
	'obs' => 'Observations',
	'amenities' => 'Amenities',
	'bookings' => [
		'title' => 'Bookings',
		'rates' => "You'll find the best rates in our",
		'bookingpage' => 'booking page',
		'phone' => 'Telephone'
	],
	'photos' => 'Photos'
];