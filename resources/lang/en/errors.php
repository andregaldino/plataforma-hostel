<?php 
return [
	'gallery' => [
		'noimage' => 'No images found',
		'nogallery' => 'No galleries found',
	],
	'pagenotfound' => 'The page you are looking for could not be found.',
	'internal' => 'Something went wrong. A team of highly trained kangaroos has been dispatched to deal with this situation.'
];