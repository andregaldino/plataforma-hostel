@extends('layouts/default')


@section('title')
{{trans('menus.reset')}}
@parent
@stop


@section('body')
<div class="content">
    <nav class="breadcrumb-nav grey lighten-4 z-depth-0">
        <div class="nav-wrapper">
            <div class="container">
                <div class="col s12">
                    <a href="{{ route('index') }}" class="breadcrumb grey-text"><i class="material-icons">home</i></a>
                    <a href="{{route('password.reset')}}" class="breadcrumb grey-text text-darken-3">{{trans('menus.reset')}}</a>
                </div>
            </div>
        </div>
    </nav>
    <div class="container">
        <h4 class="grey-text text-darken-3 center">{{ trans('messages.reset') }}</h4>
        <div class="row center">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">{{ trans('messages.account') }}</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            {{ trans('buttons.reset') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
@endsection