@extends('layouts/default')

@section('css')
<link rel="stylesheet" href="{{ asset('css/error.css') }}">
@endsection

@section('body')
<div class="container">
    <div class="error">
        <div class="row">
            <div class="col s12">
                <div class="center">
                    <img src="/images/error.svg">
                    <h1 class="grey-text text-darken-4">Ooops...</h1>
                    <h4 class="grey-text text-darken-4">{{ trans('errors.internal') }}</h4>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
@endsection