@extends('layouts/default')

@section('title')
{{trans('menus.booking')}}
@parent
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('css/booking.css') }}">
@endsection

@section('body')
<div class="content">
	<nav class="breadcrumb-nav grey lighten-4 z-depth-0">
		<div class="nav-wrapper">
			<div class="container">
				<div class="col s12">
					<a href="{{route('index')}}" class="breadcrumb grey-text">Home</a>
					<a href="{{route('booking')}}" class="breadcrumb grey-text text-darken-3">{{trans('menus.booking')}}</a>
				</div>
			</div>
		</div>
	</nav>
	<div class="booking">
		<iframe id="hqbeds" src="{{$urlFrame}}" frameborder="0" height="100%" width="100%"></iframe>
	</div>
</div>
@endsection

@section('script')
<script src="{{ asset('js/booking.js') }}"></script>
@endsection