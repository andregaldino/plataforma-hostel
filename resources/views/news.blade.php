@extends('layouts/default')

@section('css')
<link rel="stylesheet" href="{{ asset('css/blog.css') }}">
@endsection

@section('body')
<div class="container" id="startPosts">

	<div class="row">
		@forelse($feeds as $post)
		<div class="col s12 posts">			
			<div class="section">
				<a href="{{$post->get_permalink()}}" target="__blank">
					<h4 class="grey-text text-darken-3">{{$post->get_title()}}</h4>
				</a>		

				<h6 class="grey-text text-darken-1 light justify">{{str_limit($post->get_description(),200)}}</h6>

				<a href="{{$post->get_permalink()}}" target="__blank"><h5 class="grey-text text-darken-1 light center read">{{ trans('messages.read') }}</h5>
				</a>

				<div class="left">
					<h6 class="grey-text text-darken-1 light center"><i class="material-icons left">access_time</i>{{$post->get_date('j F Y | g:i a')}} </h6>
				</div>
				<div class="right">
					@if($post->get_category())
					<div class="chip">{{$post->get_category()->get_label()}}</div>
					@endif					
				</div>
			</div>
			<br>
			<div class="divider"></div>
		</div>
		@empty
		NENHUM POST ENCONTRADO NESSA CATEGORIA
		@endforelse
	</div>

	<div class="row">
		<div class="col s12">
			@forelse($feeds as $post)
			<div class="post-preview">
				<a href="{{$post->get_permalink()}}" target="__blank">
					<h4 class="post-title titleFont">
						{{$post->get_title()}}
					</h4>
				</a>
				<h6 class="post-subtitle subTitleFont">
					{{str_limit($post->get_description(),200)}}
				</h6>
				<div class="row">
					<div class="col s6">
						<p class="post-meta"><i class="material-icons left">access_time</i>{{$post->get_date('j F Y | g:i a')}} 
						</div>
						<div class="col s6">
							@if($post->get_category())
							<div class="chip right">{{$post->get_category()->get_label()}}</div>
							@endif
						</div>
					</div>
				</div>
				<div class="divider"></div>
				@empty
				NENHUM POST ENCONTRADO NESSA CATEGORIA
				@endforelse	
			</div>
		</div>
	</div>
	@endsection

	@section('script')
	<script src="{{ asset('js/news.js') }}"></script>
	@endsection