@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Funcionarios</h6>
	</div>

	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="fixed-action-btn">
	<a href="{{route('dash.staff.create')}}" class="btn-floating btn-large">
			<i class="large material-icons">add</i>
		</a>
	</div>

	<div class="container section">
		<div class="row">
			<div class="col s12">				
				<table class="striped">
					<thead>
						<tr>
							<th data-field="name">Nome</th>
							<th data-field="description">Descrição</th>
							<th data-field="created_at">Data de alteração</th>
							<th data-field="actions" class="right">Ações</th>
						</tr>
					</thead>

					<tbody>
						@forelse($staffs as $staff)
						<tr>
							<td>{{$staff->name}}</td>
							<td>{{$staff->description}}</td>
							<td>{{$staff->created_at}}</td>
							<td class="right">
								<a href="{{ route('dash.staff.delete',$staff->id) }}"><i class="material-icons actions right tooltipped" data-position="left" data-delay="50" data-tooltip="Excluir">delete</i></a>
								<a href="{{ route('dash.staff.edit',$staff->id) }}"><i class="material-icons actions right tooltipped" data-position="left" data-delay="50" data-tooltip="Editar">edit</i></a>
							</td>
						</tr>
						@empty
						<tr>
							<td colspan="4">
								No else staffs
							</td>
						</tr>
						@endforelse
					</tbody>
				</table>
				<div class="center">
					{{$staffs->links('vendor.pagination.materialize-pagination')}}
				</div>
			</div>
		</div>
	</div>
</main>
@endsection

@section('script')
@endsection