@extends('admin/layouts/default')

@section('css')
<link rel="stylesheet" href="{{ asset('css/jquery.filer.css') }}">
<link rel="stylesheet" href="{{ asset('css/gallery.css') }}">
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Novo funcionario</h6>
	</div>

	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="container section">
		<div class="row">
			<form id="staff" class="col s12" method="post" action="{{ route('dash.staff.store') }}" enctype="multipart/form-data">
				{{csrf_field()}}

				<div class="row">
					<div class="input-field col s12">
						<input id="name" type="text" name="name" value="{{ old('name') }}">
						<label for="name">Nome</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<textarea name="description" id="description" rows="10">{{ old('description') }}</textarea>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input type="file" name="pic" id="filer_input">
					</div>
				</div>

				<div class="row center">
					<div class="col s12">
						<button class="waves-effect btn black-text" type="submit">Enviar</button>
					</div>
				</div>
			</form> 
		</div>
	</div>
</main>
@endsection

@section('script')
<script src="{{ asset('js/jquery.filer.min.js') }}"></script>
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('js/staff.js') }}"></script>
@endsection