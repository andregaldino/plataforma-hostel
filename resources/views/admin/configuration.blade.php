@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Configurações</h6>
	</div>

	<div class="container section">
		<div class="row">
			<form method="post" class="col s12" action="{{ route('dash.config.store') }}">
				{{ csrf_field() }}
				<div class="row">
					<div class="input-field col s12">
						<input id="name" type="text" name="name" value="{{$configuration->name}}">
						<label for="name">Nome</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="rss" type="text" name="rss" value="{{$configuration->rss}}">
						<label for="rss">RSS</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="facebook" type="text" name="facebook" value="{{$configuration->facebook}}">
						<label for="facebook">Facebook</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="instagram" type="text" name="instagram" value="{{$configuration->instagram}}">
						<label for="instagram">Instagram</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="twitter" type="text" name="twitter" value="{{$configuration->twitter}}">
						<label for="twitter">Twitter</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="gplus" type="text" name="gplus" value="{{$configuration->gplus}}">
						<label for="gplus">Google Plus</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="city" type="text" name="city" value="{{$configuration->cidade}}">
						<label for="city">Cidade</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="state" type="text" name="state" value="{{$configuration->estado}}">
						<label for="state">Estado</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="country" type="text" name="country" value="{{$configuration->pais}}">
						<label for="country">País</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="address" type="text" name="address" value="{{$configuration->endereco}}">
						<label for="address">Endereço</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="number" type="text" name="number" value="{{$configuration->numero}}">
						<label for="number">Número</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="neighborhood" type="text" name="neighborhood" value="{{$configuration->bairro}}">
						<label for="neighborhood">Bairro</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="zip" type="text" name="zip" value="{{$configuration->cep}}">
						<label for="zip">CEP</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="beach" type="text" name="beach" value="{{$configuration->praia}}">
						<label for="beach">Praia</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="phone" type="text" name="phone" value="{{$configuration->telefone}}">
						<label for="phone">Telefone</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="whatsapp" type="text" name="whatsapp" value="{{$configuration->whatsapp}}">
						<label for="whatsapp">Whatsapp</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="email" type="text" name="email" value="{{$configuration->email}}">
						<label for="email">E-mail</label>
					</div>
				</div>

				<div class="row center">
					<div class="col s12">
						<button class="waves-effect btn black-text" type="submit">Enviar</button>
					</div>
				</div>
			</form> 
		</div>
	</div>
</main>
@endsection

@section('script')
@endsection