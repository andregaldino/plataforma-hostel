@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Editar ícone</h6>
	</div>

	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="container section">
		<div class="row">
			<form class="col s12" method="post" action="{{ route('dash.icon.update', $icon->id) }}" enctype="multipart/form-data">
				{{ csrf_field() }}
				{{ method_field('PUT')}}
				<input type="hidden" name="icon_id" value="{{$icon->id}}">
				<div class="row">
					<div class="input-field col s12">
						<input id="name" type="text" name="name" value="{{ $icon->name }}">
						<label for="name">Nome</label>
					</div>
				</div>

				<div class="row">
					<div class="file-field input-field col s12">
						<div class="btn">
							<span class="black-text">Procurar</span>
							<input type="file" name="path">
						</div>
						<div class="file-path-wrapper">
							<input class="file-path validate" type="text" placeholder="Adicionar imagem para o ícone" >
						</div>
					</div>
				</div>

				<div class="row center">
					<div class="col s12">
						<button class="waves-effect btn black-text" type="submit">Enviar</button>
					</div>
				</div>
			</form> 
		</div>
	</div>
</main>
@endsection

@section('script')
@endsection