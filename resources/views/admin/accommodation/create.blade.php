@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Nova acomodação</h6>
	</div>
	<div class="notifications">
		@include('errors/notifications')
	</div>
	<div class="container section">
		<div class="row">
			<div class="col s12">
				<ul class="tabs">
					<li class="tab col s4"><a class="active" href="#portuguese">Português</a></li>
					<li class="tab col s4"><a href="#english">Inglês</a></li>
					<li class="tab col s4"><a href="#spanish">Espanhol</a></li>
				</ul>
			</div>
			<br>
			<br>
			<form method="post" action="{{ route('dash.accommodation.store') }}">
				{{ csrf_field() }}
				<div id="portuguese">					
					<div class="row">
						<div class="input-field col s12">
							<input id="title" type="text" name="portuguese_title" value="{{ old('portuguese_title') }}">
							<label for="title">Título</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<input id="subtitle" type="text" name="portuguese_subtitle" value="{{ old('portuguese_subtitle') }}">
							<label for="subtitle">Subtítulo</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="description" class="materialize-textarea" name="portuguese_description"  >{{ old('portuguese_description') }}</textarea>
							<label for="description">Descrição</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="observations" class="materialize-textarea" name="portuguese_observations" >{{ old('portuguese_observations') }}</textarea>
							<label for="observations">Observações</label>
						</div>
					</div>					
				</div>

				<div id="english">
					
					<div class="row">
						<div class="input-field col s12">
							<input id="title" type="text" name="english_title" value="{{ old('english_title') }}">
							<label for="title">Título</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<input id="subtitle" type="text" name="english_subtitle" value="{{ old('english_subtitle') }}">
							<label for="subtitle">Subtítulo</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="description" class="materialize-textarea" name="english_description">{{ old('english_description') }}</textarea>
							<label for="description">Descrição</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="observations" class="materialize-textarea" name="english_observations">{{ old('english_observations') }}</textarea>
							<label for="observations">Observações</label>
						</div>
					</div>
				</div>

				<div id="spanish">
					<div class="row">
						<div class="input-field col s12">
							<input id="title" type="text" name="spanish_title" value="{{ old('spanish_title') }}">
							<label for="title">Título</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<input id="subtitle" type="text" name="spanish_subtitle" value="{{ old('spanish_subtitle') }}">
							<label for="subtitle">Subtítulo</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="description" class="materialize-textarea" name="spanish_description" >{{ old('spanish_description') }}</textarea>
							<label for="description">Descrição</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="observations" class="materialize-textarea" name="spanish_observations">{{ old('spanish_observations') }}</textarea>
							<label for="observations">Observações</label>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<label>Numero de camas da Acomodação</label><br><br>
						<input type="number" min="1" id="beds" name="beds" value="{{ old('beds') }}" />
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<label>Genero da Acomodação</label><br><br>
						<select name="gender">
							<option value="" disabled selected>Selecione um tipo</option>
							<option value="mixed">Misto</option>
							<option value="female">Feminino</option>
							<option value="male">Masculino</option>
						</select>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<label>Serviços oferecidos para a Acomodação</label><br><br>
						@forelse($services as $service)
						<input type="checkbox" id="{{$service->id}}" name="services[]" value="{{$service->id}}" />
						<label for="{{$service->id}}">{{$service->translation()->name}}</label>
						@empty
						Nenhum
						@endforelse
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<select name="type_id">
							<option value="" disabled selected>Selecione um tipo</option>
							@forelse($types as $type)
							<option value="{{$type->id}}">{{$type->translation()->name}}</option>
							@empty
							@endforelse
						</select>
						<label>Tipo da acomodação</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<select name="gallery_id">
							<option value="" disabled selected>Selecione uma Galeria de imagens</option>
							@forelse($galleries as $gallery)
							<option value="{{$gallery->id}}">{{$gallery->title}}</option>
							@empty
							@endforelse
						</select>
						<label>Galeria de Imagens</label>
					</div>
				</div>

				<div class="row center">
					<div class="col s12">
						<button class="waves-effect btn black-text" type="submit">Enviar</button>
					</div>
				</div>
			</form> 


		</div>
	</div>
</main>
@endsection

@section('script')
@endsection