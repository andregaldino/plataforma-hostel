@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Editar acomodação</h6>
	</div>
	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="container section">
		<div class="row">
			<div class="col s12">
				<ul class="tabs">
					<li class="tab col s4"><a class="active" href="#portuguese">Português</a></li>
					<li class="tab col s4"><a href="#english">Inglês</a></li>
					<li class="tab col s4"><a href="#spanish">Espanhol</a></li>
				</ul>
			</div>
			<form method="post" action="{{ route('dash.accommodation.update', $accommodation->id) }}" class="col s12">

				{{ csrf_field() }}

				{{ method_field('PUT')}}
				<div id="portuguese" class="col s12">
					
					<input type="hidden" name="pt_accommodation_translation_id" value="{{$accommodation->translation('pt')->id}}">
					<div class="row">
						<div class="input-field col s12">
							<input id="title" type="text" name="portuguese_title" value="{{$accommodation->translation('pt')->title}}">
							<label for="title">Título</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<input id="subtitle" type="text" name="portuguese_subtitle" value="{{$accommodation->translation('pt')->subtitle}}">
							<label for="subtitle">Subtítulo</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="description" class="materialize-textarea" name="portuguese_description">{{$accommodation->translation('pt')->description}}</textarea>
							<label for="description">Descrição</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="observations" class="materialize-textarea" name="portuguese_observations">{{$accommodation->translation('pt')->observations}}</textarea>
							<label for="observations">Observações</label>
						</div>
					</div>
				</div>

				<div id="english" class="col s12">
					<input type="hidden" name="en_accommodation_translation_id" value="{{$accommodation->translation('en')->id}}">
					<div class="row">
						<div class="input-field col s12">
							<input id="title" type="text" name="english_title" value="{{$accommodation->translation('en')->title}}">
							<label for="title">Título</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<input id="subtitle" type="text" name="english_subtitle" value="{{$accommodation->translation('en')->subtitle}}">
							<label for="subtitle">Subtítulo</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="description" class="materialize-textarea" name="english_description">{{$accommodation->translation('en')->description}}</textarea>
							<label for="description">Descrição</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="observations" class="materialize-textarea" name="english_observations">{{$accommodation->translation('en')->observations}}</textarea>
							<label for="observations">Observações</label>
						</div>
					</div>

				</div>

				<div id="spanish" class="col s12">
					<input type="hidden" name="es_accommodation_translation_id" value="{{$accommodation->translation('es')->id}}">
					<div class="row">
						<div class="input-field col s12">
							<input id="title" type="text" name="spanish_title" value="{{$accommodation->translation('es')->title}}">
							<label for="title">Título</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<input id="subtitle" type="text" name="spanish_subtitle" value="{{$accommodation->translation('es')->subtitle}}">
							<label for="subtitle">Subtítulo</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="description" class="materialize-textarea" name="spanish_description">{{$accommodation->translation('es')->description}}</textarea>
							<label for="description">Descrição</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="observations" class="materialize-textarea" name="spanish_observations">{{$accommodation->translation('es')->observations}}</textarea>
							<label for="observations">Observações</label>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<label>Numero de camas da Acomodação</label><br><br>
						<input type="number" min="1" value="{{$accommodation->beds}}" id="beds" name="beds" />
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<label>Genero da Acomodação</label><br><br>
						<select name="gender">
							<option value="" disabled selected>Selecione um tipo</option>
							@if($accommodation->gender == "mixed")
								<option value="mixed" selected>Misto</option>
								<option value="female">Feminino</option>
								<option value="male">Masculino</option>
							@elseif($accommodation->gender == "female")
								<option value="mixed">Misto</option>
								<option value="female" selected>Feminino</option>
								<option value="male">Masculino</option>
							@elseif($accommodation->gender == "male")
								<option value="mixed">Misto</option>
								<option value="female">Feminino</option>
								<option value="male" selected>Masculino</option>
							@endif
						</select>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<label>Serviços</label><br><br>
						@forelse($services as $service)
						@if($accommodation->services->contains('id',$service->id))
						<input type="checkbox" name="services[]" value="{{$service->id}}" checked id="{{$service->id}}" />
						<label for="{{$service->id}}">{{$service->translation()->name}}</label>
						@else
						<input type="checkbox" name="services[]" value="{{$service->id}}" id="{{$service->id}}" />
						<label for="{{$service->id}}">{{$service->translation()->name}}</label>
						@endif
						@empty
						Nenhum
						@endforelse
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<select name="type_id">
							<option value="" disabled selected>Selecione um tipo</option>
							@forelse($types as $type)
							@if($accommodation->type->id == $type->id)
							<option value="{{$type->id}}" selected>{{$type->translation()->name}}</option>
							@else
							<option value="{{$type->id}}">{{$type->translation()->name}}</option>
							@endif
							@empty
							@endforelse
						</select>
						<label>Tipo</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<select name="gallery_id">
							<option value="" disabled selected>Selecione uma Galeria de imagens</option>
							@forelse($galleries as $gallery)
								@if(isset($accommodation->gallery) && $accommodation->gallery->id == $gallery->id)
									<option value="{{$gallery->id}}" selected>{{$gallery->title}}</option>
								@else
									<option value="{{$gallery->id}}">{{$gallery->title}}</option>
								@endif
							@empty
							@endforelse
						</select>
						<label>Galeria de Imagens</label>
					</div>
				</div>

				<div class="row center">
					<div class="col s12">
						<button class="waves-effect btn black-text" type="submit">Enviar</button>
					</div>
				</div>
			</form> 
		</div>
	</div>
</main>
@endsection

@section('script')
@endsection