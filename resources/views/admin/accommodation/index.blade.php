@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Acomodações</h6>
	</div>

	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="fixed-action-btn">
	<a href="{{route('dash.accommodation.create')}}" class="btn-floating btn-large">
			<i class="large material-icons">add</i>
		</a>
	</div>

	<div class="container section">
		<div class="row">
			<div class="col s12">				
				<table class="striped">
					<thead>
						<tr>
							<th data-field="name">Nome</th>
							<th data-field="type">Tipo</th>
							<th data-field="type">Serviços</th>
							<th data-field="gender">Genero</th>
							<th data-field="beds">Qtd de Camas</th>
							<th data-field="actions" class="right">Ações</th>
						</tr>
					</thead>

					<tbody>
						@forelse($accommodations as $accommodation)
						<tr>
							<td>{{$accommodation->translation()->title}}</td>
							<td>{{$accommodation->type->translation()->name}}</td>
							<td>{{$accommodation->services->count()}}</td>
							<td>{{$accommodation->gender}}</td>
							<td>{{$accommodation->beds}}</td>
							<td class="right">
								@if (Sentinel::getUser()->hasAccess('accommodation.delete'))
								<a href="{{ route('dash.accommodation.delete',$accommodation->id) }}"><i class="material-icons actions right tooltipped" data-position="left" data-delay="50" data-tooltip="Excluir">delete</i></a>
								@endif
								@if (Sentinel::getUser()->hasAccess('accommodation.edit'))
								<a href="{{ route('dash.accommodation.edit',$accommodation->id) }}"><i class="material-icons actions right tooltipped" data-position="left" data-delay="50" data-tooltip="Editar">edit</i></a>
								@endif
							</td>
						</tr>
						@empty
						<tr>
							<td colspan="6">
								No else accommodations
							</td>
						</tr>
						@endforelse
					</tbody>
				</table>
				<div class="center">
					{{$accommodations->links('vendor.pagination.materialize-pagination')}}
				</div>
			</div>
		</div>
	</div>
</main>
@endsection

@section('script')
@endsection