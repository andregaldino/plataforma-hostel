@extends('admin/layouts/default')

@section('css')
<link rel="stylesheet" href="{{ asset('css/materialNote.css') }}">

@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Editar postagem</h6>
	</div>

	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="container section">
		<div class="row">
			<form class="col s12" method="post" action="{{ route('dash.post.update', $post->id) }}" enctype="multipart/form-data">
				{{csrf_field()}}
				{{ method_field('PUT')}}
				<input type="hidden" name="post_id" value="{{$post->id}}">

				<div class="row">
					<div class="input-field col s12">
						<input id="title" type="text" name="title" value="{{$post->title}}">
						<label for="title">Título</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<textarea name="body" id="body" rows="10">{{ $post->body }}</textarea>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<select name="category_id">
							<option value="" disabled selected>Selecione uma categoria</option>
							@forelse($categories as $category)
							@if($post->category->id == $category->id)
							<option value="{{$category->id}}" selected>{{$category->name}}</option>
							@else
							<option value="{{$category->id}}">{{$category->name}}</option>
							@endif
							
							@empty
							@endforelse
						</select>
						<label>Categoria</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<select name="gallery_id">
							<option value="" disabled selected>Selecione uma Galeria de imagens</option>
							@forelse($galleries as $gallery)
								@if(isset($post->gallery) && $post->gallery->id == $gallery->id)
									<option value="{{$gallery->id}}" selected>{{$gallery->title}}</option>
								@else
									<option value="{{$gallery->id}}">{{$gallery->title}}</option>
								@endif
							@empty
							@endforelse
						</select>
						<label>Galeria de Imagens</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="youtube" type="text" name="youtube">
						<label for="youtube">Youtube</label>
					</div>
				</div>


				<div class="row center">
					<div class="col s12">
						<button class="waves-effect btn black-text" type="submit">Enviar</button>
					</div>
				</div>
			</form> 
		</div>
	</div>
</main>
@endsection

@section('script')
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('js/post.js') }}"></script>
@endsection