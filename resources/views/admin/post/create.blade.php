@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Nova postagem</h6>
	</div>


	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="container section">
		<div class="row">
			<form id="post" class="col s12" method="post" action="{{ route('dash.post.store') }}"  enctype="multipart/form-data">
				{{csrf_field()}}

				<div class="row">
					<div class="input-field col s12">
						<input id="title" type="text" name="title" value="{{ old('title') }}">
						<label for="title">Título</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<textarea name="body" id="body" rows="10">{{ old('body') }}</textarea>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<select name="category_id">
							<option value="" disabled selected>Selecione uma categoria</option>
							@forelse($categories as $category)
							<option value="{{$category->id}}">{{$category->name}}</option>
							@empty
							@endforelse
						</select>
						<label>Categoria</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<select name="gallery_id">
							<option value="" disabled selected>Selecione uma Galeria de imagens</option>
							@forelse($galleries as $gallery)
							<option value="{{$gallery->id}}">{{$gallery->title}}</option>
							@empty
							@endforelse
						</select>
						<label>Galeria de Imagens</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="youtube" type="text" name="youtube" value="{{ old('youtube') }}">
						<label for="youtube">Youtube</label>
					</div>
				</div>

				<div class="row">
					<div class="file-field input-field col s12">
						<div class="btn">
							<span class="black-text">Procurar</span>
							<input type="file" name="pic">
						</div>
						<div class="file-path-wrapper">
							<input class="file-path validate" type="text" placeholder="Adicionar imagem para o Post" >
						</div>
					</div>
				</div>


				<div class="row center">
					<div class="col s12">
						<button class="waves-effect btn black-text" type="submit">Postar</button>
					</div>
				</div>
			</form> 
		</div>
	</div>
</main>
@endsection

@section('script')
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('js/post.js') }}"></script>
@endsection