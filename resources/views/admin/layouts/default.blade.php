<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>

	<title>
		@section('title') - Hostel Dashboard
		@show
	</title>

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	<meta name="theme-color" content="#212121" />
	<link rel="shortcut icon" href="favicon.png">

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="{{ asset('css/materialize.css') }}">
	<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/admin.css') }}">

	@yield('css')
</head>
<body>
	<div class="page">
		@include('admin/layouts/nav')
		
		@yield('body')
	</div>

	<script src="{{ asset('js/jquery-2.1.4.js') }}"></script>
	<script src="{{ asset('js/materialize.js') }}"></script>
	<script src="{{ asset('js/admin.js') }}"></script>
	@yield('script')
</body>
</html>