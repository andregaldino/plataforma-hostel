@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Novo Evento</h6>
	</div>

	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="container section">
		<div class="row">
			<div class="col s12">
				<ul class="tabs">
					<li class="tab col s4"><a class="active" href="#portuguese">Português</a></li>
					<li class="tab col s4"><a href="#english">Inglês</a></li>
					<li class="tab col s4"><a href="#spanish">Espanhol</a></li>
				</ul>
			</div>

			<form class="col s12" method="post" action="{{ route('dash.event.store') }}"  enctype="multipart/form-data">
				{{ csrf_field() }}
				<div id="portuguese">
					<div class="row">
						<div class="input-field col s12">
							<input id="title" type="text" name="portuguese_title" value="{{ old('portuguese_title') }}">
							<label for="title">Título</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="portuguese_description" name="portuguese_description">{{ old('portuguese_description') }}</textarea>
						</div>
					</div>
				</div>

				<div id="english">
					<div class="row">
						<div class="input-field col s12">
							<input id="title" type="text" name="english_title" value="{{ old('english_title') }}">
							<label for="title">Título</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="english_description" name="english_description">{{ old('english_description') }}</textarea>
						</div>
					</div>
				</div>

				<div id="spanish">
					<div class="row">
						<div class="input-field col s12">
							<input id="title" type="text" name="spanish_title" value="{{ old('spanish_title') }}">
							<label for="title">Título</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="spanish_description" name="spanish_description">{{ old('spanish_description') }}</textarea>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="date" type="date" class="datepicker" name="start_date" value="{{ old('start_date') }}">
						<label for="date">Data de início</label>
					</div>
				</div>
				
				<div class="row">
					<div class="input-field col s12">
						<select name="gallery_id">
							<option value="" disabled selected>Selecione uma Galeria de imagens</option>
							@forelse($galleries as $gallery)
							<option value="{{$gallery->id}}">{{$gallery->title}}</option>
							@empty
							@endforelse
						</select>
						<label>Galeria de Imagens</label>
					</div>
				</div>

				<div class="row">
					<div class="file-field input-field col s12">
						<div class="btn">
							<span class="black-text">Procurar</span>
							<input type="file" name="pic">
						</div>
						<div class="file-path-wrapper">
							<input class="file-path validate" type="text" placeholder="Adicionar imagem para o evento" >
						</div>
					</div>
				</div>

				<div class="row center">
					<div class="col s12">
						<button class="waves-effect btn black-text" type="submit">Postar</button>
					</div>
				</div>
			</form> 
		</div>
	</div>
</main>
@endsection

@section('script')
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('js/event.js') }}"></script>
@endsection