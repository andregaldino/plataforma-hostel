@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Serviços</h6>
	</div>

	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="fixed-action-btn">
	<a href="{{route('dash.service.create')}}" class="btn-floating btn-large">
			<i class="large material-icons">add</i>
		</a>
	</div>

	<div class="container section">
		<div class="row">
			<div class="col s12">				
				<table class="striped">
					<thead>
						<tr>
							<th data-field="name">Nome</th>
							<th data-field="icon">Icone</th>
							<th data-field="actions" class="right">Ações</th>
						</tr>
					</thead>

					<tbody>
						@forelse($services as $service)
						<tr>
							<td>{{$service->translation('pt')->name}}</td>
							<td> <img class="icon" src="{{ asset('images/icons/'.$service->icon) }}"></td>
							<td class="right">
								<a href="{{ route('dash.service.delete',$service->id) }}"><i class="material-icons actions right tooltipped" data-position="left" data-delay="50" data-tooltip="Excluir">delete</i></a>
								<a href="{{ route('dash.service.edit',$service->id) }}"><i class="material-icons actions right tooltipped" data-position="left" data-delay="50" data-tooltip="Editar">edit</i></a>
							</td>
						</tr>
						@empty
						<tr>
							<td colspan="3">
								No else services
							</td>
						</tr>
						@endforelse
					</tbody>
				</table>
				<div class="center">
					{{$services->links('vendor.pagination.materialize-pagination')}}
				</div>
			</div>
		</div>
	</div>
</main>
@endsection

@section('script')
@endsection