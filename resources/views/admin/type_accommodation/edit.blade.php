@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Editar tipo de acomodação</h6>
	</div>

	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="container section">
		<div class="row">
			<div class="col s12">
				<ul class="tabs">
					<li class="tab col s4"><a class="active" href="#portuguese">Português</a></li>
					<li class="tab col s4"><a href="#english">Inglês</a></li>
					<li class="tab col s4"><a href="#spanish">Espanhol</a></li>
				</ul>
			</div>

			<form method="post" action="{{ route('dash.type_accommodation.update', $type_accommodation->id) }}" class="col s12">
				{{ csrf_field() }}
				{{ method_field('PUT')}}
				<div id="portuguese" class="col s12">
					<input type="hidden" name="type_accommodation_id" value="{{$type_accommodation->id}}">
					<input type="hidden" name="pt_type_translation_id" value="{{$type_accommodation->translation('pt')->id}}">
					<div class="row">
						<div class="input-field col s12">
							<input id="name" type="text" name="portuguese_name" value="{{$type_accommodation->translation('pt')->name}}">
							<label for="name">Nome</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="description" class="materialize-textarea" name="portuguese_description">{{$type_accommodation->translation('pt')->description}}</textarea>
							<label for="description">Descrição</label>
						</div>
					</div>

				</div>

				<div id="english" class="col s12">
					<input type="hidden" name="type_accommodation_id" value="{{$type_accommodation->id}}">
					<input type="hidden" name="en_type_translation_id" value="{{$type_accommodation->translation('en')->id}}">
					<div class="row">
						<div class="input-field col s12">
							<input id="name" type="text" name="english_name" value="{{$type_accommodation->translation('en')->name}}">
							<label for="name">Nome</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="description" class="materialize-textarea" name="english_description">{{$type_accommodation->translation('en')->description}}</textarea>
							<label for="description">Descrição</label>
						</div>
					</div>
				</div>

				<div id="spanish" class="col s12">
						<input type="hidden" name="type_accommodation_id" value="{{$type_accommodation->id}}">
						<input type="hidden" name="es_type_translation_id" value="{{$type_accommodation->translation('es')->id}}">
						<div class="row">
							<div class="input-field col s12">
								<input id="name" type="text" name="spanish_name" value="{{$type_accommodation->translation('es')->name}}">
								<label for="name">Nome</label>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12">
								<textarea id="description" class="materialize-textarea" name="spanish_description">{{$type_accommodation->translation('es')->description}}</textarea>
								<label for="description">Descrição</label>
							</div>
						</div>
					</div>


					<div class="row center">
						<div class="col s12">
							<button class="waves-effect btn black-text" type="submit">Enviar</button>
						</div>
					</div>
				</form> 

			</div>
		</div>
	</main>
	@endsection

	@section('script')
	@endsection