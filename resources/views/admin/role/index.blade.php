@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Permissões</h6>
	</div>

	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="fixed-action-btn">
	<a href="{{route('dash.role.create')}}" class="btn-floating btn-large">
			<i class="large material-icons">add</i>
		</a>
	</div>

	<div class="container section">
		<div class="row">
			<div class="col s12">				
				<table class="striped">
					<thead>
						<tr>
							<th data-field="name">Nome</th>
							<th data-field="users">Qtd de Usuários</th>
							<th data-field="actions" class="right">Ações</th>
						</tr>
					</thead>

					<tbody>
						@forelse($roles as $role)
						<tr>
							<td>{{$role->name}}</td>
							<td>{{$role->users()->count()}}</td>
							<td class="right">
								<a href="{{ route('dash.role.delete',$role->id) }}"><i class="material-icons actions right tooltipped" data-position="left" data-delay="50" data-tooltip="Excluir">delete</i></a>
								<a href="{{ route('dash.role.edit',$role->id) }}"><i class="material-icons actions right tooltipped" data-position="left" data-delay="50" data-tooltip="Editar">edit</i></a>
							</td>
						</tr>
						@empty
						<tr>
							<td colspan="6">
								No else roles
							</td>
						</tr>
						@endforelse
					</tbody>
				</table>
				<div class="center">
					{{$roles->links('vendor.pagination.materialize-pagination')}}
				</div>
			</div>
		</div>
	</div>
</main>
@endsection

@section('script')
@endsection