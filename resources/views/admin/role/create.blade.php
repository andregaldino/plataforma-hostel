@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Novo grupo de permissões</h6>
	</div>
	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="container section">
		<div class="row">
			<form method="post" action="{{ route('dash.role.store') }}" class="col s12">
				{{ csrf_field() }}
				<div class="row">
					<div class="input-field col s12">
						<input id="name" type="text" name="name" value="{{ old('name') }}">
						<label for="name">Nome do grupo</label>
					</div>
				</div>

				@include('admin/role/permissions')

				<br>

				<div class="row center">
					<div class="col s12">
						<button class="waves-effect btn black-text" type="submit">Enviar</button>
					</div>
				</div>
			</form> 
		</div>
	</div>
</main>
@endsection

@section('script')
<script src="{{ asset('js/roles.js') }}"></script>
@endsection