@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Editar usuário</h6>
	</div>
	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="container section">
		<div class="row">
			<form method="post" action="{{ route('dash.user.update', $user->id) }}" class="col s12">
				{{ csrf_field() }}
				{{ method_field('PUT')}}
				<input type="hidden" name="user_id" value="{{$user->id}}">
				<div class="row">
					<div class="input-field col s12">
						<input id="name" type="text" name="name" value="{{$user->name}}">
						<label for="name">Nome</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="email" type="email" name="email" value="{{$user->email}}">
						<label for="email">E-mail</label>
					</div>
				</div>
				
				<div class="row">
					<div class="input-field col s12">
						<select name="roles">
							<option value="" disabled selected>Selecione o grupo</option>
							@forelse($roles as $role)
								@if($user->roles[0]->id == $role->id)
									<option value="{{$role->id}}" selected>{{$role->name}}</option>
								@else
									<option value="{{$role->id}}">{{$role->name}}</option>
								@endif
							@empty

							@endforelse
						</select>
						<label>Grupo</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="password" type="password" name="password" class="validate">
						<label for="password">Senha</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="password_confirmation" type="password" name="password_confirmation" class="validate">
						<label for="password_confirmation">Confirmar senha</label>
					</div>
				</div>

				<div class="row">
					<div class="col s12">		
						<label>Status</label>
						<div class="switch">
							<label>
								Desativado
								<input type="checkbox">
								<span class="lever"></span>
								Ativo
							</label>
						</div>
					</div>
				</div>

				<div class="row center">
					<div class="col s12">
						<button class="waves-effect btn black-text" type="submit">Enviar</button>
					</div>
				</div>
			</form> 
		</div>
	</div>
</main>
@endsection

@section('script')
@endsection