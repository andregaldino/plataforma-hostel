@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Categorias</h6>
	</div>

	<div class="notifications">
		@include('errors/notifications')
	</div>


	<div class="fixed-action-btn">
	<a href="{{route('dash.category.create')}}" class="btn-floating btn-large">
			<i class="large material-icons">add</i>
		</a>
	</div>
	
	<div class="container section">
		<div class="row">
			<div class="col s12">				
				<table class="striped">
					<thead>
						<tr>
							<th data-field="name">Nome</th>
							<th data-field="posts">Quantidade Posts</th>
							<th data-field="actions" class="right">Ações</th>
						</tr>
					</thead>

					<tbody>
						@forelse($categories as $category)
						<tr>
							<td>{{$category->translation('pt')->name}}</td>
							<td>{{$category->posts->count()}}</td>
							<td class="right">
								<a href="{{ route('dash.category.delete',$category->id) }}"><i class="material-icons actions right tooltipped" data-position="left" data-delay="50" data-tooltip="Excluir">delete</i></a>
								<a href="{{ route('dash.category.edit',$category->id) }}"><i class="material-icons actions right tooltipped" data-position="left" data-delay="50" data-tooltip="Editar">edit</i></a>
							</td>
						</tr>
						@empty
						<tr>
							<td colspan="3">
								No else categories
							</td>
						</tr>
						@endforelse
					</tbody>
				</table>
				<div class="center">
					{{$categories->links('vendor.pagination.materialize-pagination')}}
				</div>
			</div>
		</div>
	</div>
</main>
@endsection

@section('script')
@endsection