@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Dashboard</h6>
	</div>

	<div class="notifications">
		@include('errors/notifications')
	</div>
	
	<div class="container">		
		<div class="row">
			<div class="col s12 m6 l4">
				<a href="{{ route('dash.post.index') }}">
					<div class="card z-depth--1 black-text">				
						<h3 class="center"><i class="material-icons large">description</i></h3>
						<h5 class="center">Postagens</h5>
					</div>
				</a>
			</div>
			<div class="col s12 m6 l4">
				<a href="{{ route('dash.event.index') }}">
					<div class="card z-depth--1 black-text">				
						<h2 class="center"><i class="material-icons large">event</i></h2>
						<h5 class="center">Eventos</h5>
					</div>
				</a>
			</div>
			<div class="col s12 m6 l4">
				<a href="{{ route('dash.gallery.index') }}">
					<div class="card z-depth--1 black-text">				
						<h2 class="center"><i class="material-icons large">collections</i></h2>
						<h5 class="center">Galeria de fotos</h5>
					</div>
				</a>
			</div>
			<div class="col s12 m6 l4">
				<a href="{{ route('dash.accommodation.index') }}">
					<div class="card z-depth--1 black-text">				
						<h2 class="center"><i class="material-icons large">hotel</i></h2>
						<h5 class="center">Acomodações</h5>
					</div>
				</a>
			</div>
			<div class="col s12 m6 l4">
				<a href="{{ route('dash.staff.index') }}">
					<div class="card z-depth--1 black-text">				
						<h2 class="center"><i class="material-icons large">people</i></h2>
						<h5 class="center">Staff</h5>
					</div>
				</a>
			</div>
			<div class="col s12 m6 l4">
				<a href="{{ route('dash.config') }}">
					<div class="card z-depth--1 black-text">				
						<h2 class="center"><i class="material-icons large">settings</i></h2>
						<h5 class="center">Configurações</h5>
					</div>
				</a>
			</div>
			<div class="col s12 m6 l4">
				<a href="{{ route('dash.role.index') }}">
					<div class="card z-depth--1 black-text">				
						<h2 class="center"><i class="material-icons large">lock</i></h2>
						<h5 class="center">Permissões</h5>
					</div>
				</a>
			</div>
			<div class="col s12 m6 l4">
				<a href="{{ route('dash.user.index') }}">
					<div class="card z-depth--1 black-text">				
						<h2 class="center"><i class="material-icons large">people_outline</i></h2>
						<h5 class="center">Usuários</h5>
					</div>
				</a>
			</div>
			<div class="col s12 m6 l4">
				<a href="{{ route('logout') }}">
					<div class="card z-depth--1 black-text">				
						<h2 class="center"><i class="material-icons large">exit_to_app</i></h2>
						<h5 class="center">Sair</h5>
					</div>
				</a>
			</div>
		</div>
	</div>
</main>
@endsection

@section('script')
@endsection