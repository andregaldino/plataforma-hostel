@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Nova regra</h6>
	</div>

	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="container section">
		<div class="row">
			<div class="col s12">
				<ul class="tabs">
					<li class="tab col s4"><a class="active" href="#portuguese">Português</a></li>
					<li class="tab col s4"><a href="#english">Inglês</a></li>
					<li class="tab col s4"><a href="#spanish">Espanhol</a></li>
				</ul>
			</div>
			<form class="col s12" method="post" action="{{ route('dash.term.update', $term->id) }}" enctype="multipart/form-data">
				{{ csrf_field() }}
				{{ method_field('PUT')}}
				<div id="portuguese">
					<div class="row">
						<div class="input-field col s12">
							<input id="title" type="text" name="portuguese_title" value="{{$term->translation('pt')->title}}">
							<label for="title">Título</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="portuguese_description" class="materialize-textarea" name="portuguese_description">{{$term->translation('pt')->description}}</textarea>
						</div>
					</div>
				</div>

				<div id="english">
					<div class="row">
						<div class="input-field col s12">
							<input id="title" type="text" name="english_title" value="{{$term->translation('en')->title}}">
							<label for="title">Título</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea name="english_description" id="english_description" rows="10">{{$term->translation('en')->description}}</textarea>
						</div>
					</div>
				</div>

				<div id="spanish">
					<div class="row">
						<div class="input-field col s12">
							<input id="title" type="text" name="spanish_title" value="{{$term->translation('es')->title}}">
							<label for="title">Título</label>
						</div>
					</div>

					<div class="row">
						<div class="input-field col s12">
							<textarea id="spanish_description" class="materialize-textarea" name="spanish_description">{{$term->translation('es')->description}}</textarea>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<select class="icons" name="icon">
							<option value="" disabled selected>Escola um ícone para a regra</option>
							@forelse($files as $file)
							@if($term->icon == $file->path)
							<option class="left" value="{{$file->path}}" data-icon="/images/icons/{{$file->path}}" selected>{{$file->name}}</option>
							@else
							<option class="left" value="{{$file->path}}" data-icon="/images/icons/{{$file->path}}">{{$file->name}}</option>
							@endif
							@empty

							@endforelse
						</select>
					</div>
				</div>

				<div class="row center">
					<div class="col s12">
						<button class="waves-effect btn black-text" type="submit">Enviar</button>
					</div>
				</div>
			</form> 
		</div>
	</div>
</main>
@endsection

@section('script')
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('js/term.js') }}"></script>
@endsection