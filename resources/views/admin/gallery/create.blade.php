@extends('admin/layouts/default')

@section('css')
<link rel="stylesheet" href="{{ asset('css/jquery.filer.css') }}">
<link rel="stylesheet" href="{{ asset('css/gallery.css') }}">
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Nova Galeria</h6>
	</div>

	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="container section">
		<div class="row">
			<form class="col s12" method="post" action="{{ route('dash.gallery.store') }}"  enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="row">
					<div class="input-field col s12">
						<input id="title" type="text" name="title" value="{{ old('title') }}">
						<label for="title">Título</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<textarea id="description" class="materialize-textarea" name="description">{{ old('description') }}</textarea>
						<label for="description">Descrição</label>
					</div>
				</div>

				
				<div class="row">
					<div class="input-field col s12">
						<input type="file" name="files[]" id="filer_input" multiple="multiple">
					</div>
				</div>

				<div class="row center">
					<div class="col s12">
						<button class="waves-effect btn black-text" type="submit">Postar</button>
					</div>
				</div>
			</form> 
		</div>
	</div>
</main>
@endsection

@section('script')
<script src="{{ asset('js/jquery.filer.min.js') }}"></script>
<script src="{{ asset('js/gallery.js') }}"></script>
@endsection