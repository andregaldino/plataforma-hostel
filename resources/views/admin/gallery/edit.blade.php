@extends('admin/layouts/default')

@section('css')
<link rel="stylesheet" href="{{ asset('css/jquery.filer.css') }}">
<link rel="stylesheet" href="{{ asset('css/gallery.css') }}">
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Editar Galeria</h6>
	</div>

	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="container section">
		<div class="row">
			<form class="col s12" method="post" action="{{ route('dash.gallery.update', $gallery->id) }}"  enctype="multipart/form-data">
				{{ csrf_field() }}
				{{ method_field('PUT')}}
				<input type="hidden" name="gallery_id" value="{{$gallery->id}}">
				<input id="old_files" type="hidden" name="old_files">
				<div class="row">
					<div class="input-field col s12">
						<input id="title" type="text" name="title" value="{{$gallery->title}}">
						<label for="title">Título</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<textarea id="description" class="materialize-textarea" name="description">{{$gallery->description}}</textarea>
						<label for="description">Descrição</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input type="file" name="files[]" id="filer_input_edit" multiple="multiple">
					</div>
				</div>
				<div class="row center">
					<div class="col s12">
						<button class="waves-effect btn black-text" type="submit">Postar</button>
					</div>
				</div>
			</form> 
		</div>
	</div>
</main>
@endsection

@section('script')
<script src="{{ asset('js/jquery.filer.min.js') }}"></script>
<script type="text/javascript">
	gallery = {!! json_encode($gallery->photos->toArray()) !!};
</script>
<script src="{{ asset('js/gallery.js') }}"></script>
@endsection