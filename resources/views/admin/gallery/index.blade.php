@extends('admin/layouts/default')

@section('css')
@endsection

@section('body')
<main>
	<div class="breadcrumb grey lighten-3">
		<h6>Galeria de fotos</h6>
	</div>

	<div class="notifications">
		@include('errors/notifications')
	</div>

	<div class="fixed-action-btn">
	<a href="{{route('dash.gallery.create')}}" class="btn-floating btn-large">
			<i class="large material-icons">add</i>
		</a>
	</div>
	
	<div class="container section">
		<div class="row">
			<div class="col s12">
				<table class="striped">
					<thead>
						<tr>
							<th data-field="title">Título</th>
							<th data-field="count">Quantidade de fotos</th>
							<th data-field="actions" class="right">Ações</th>
						</tr>
					</thead>

					<tbody>
						@forelse($galleries as $gallery)
						<tr>
							<td>{{$gallery->title}}</td>
							<td>{{$gallery->photos->count()}}</td>
							<td class="right">
							@if (Sentinel::getUser()->hasAccess('gallery.delete'))
								<a href="{{ route('dash.gallery.delete',$gallery->id) }}"><i class="material-icons actions right tooltipped" data-position="left" data-delay="50" data-tooltip="Excluir">delete</i></a>
							@endif
							@if (Sentinel::getUser()->hasAccess('gallery.edit'))
								<a href="{{ route('dash.gallery.edit',$gallery->id) }}"><i class="material-icons actions right tooltipped" data-position="left" data-delay="50" data-tooltip="Editar">edit</i></a>
							@endif
							</td>
						</tr>
						@empty
						<tr>
							<td colspan="3">
								No else galleries
							</td>
						</tr>
						@endforelse
					</tbody>
				</table>
				<div class="center">
					{{$galleries->links('vendor.pagination.materialize-pagination')}}
				</div>
			</div> 
		</div>
	</div>
</main>
@endsection

@section('script')
@endsection