<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>Dashboard login</title>
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	<meta name="theme-color" content="#212121" />

	<link rel="stylesheet" href="{{ asset('css/materialize.css') }}">
	<link rel="stylesheet" href="{{ asset('css/login.css') }}">
	<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
</head>
<body>
	<div class="content">
		<div class="row">
			<form class="col s12 m5 offset-m3 l4 offset-l4" method="post" action="{{ route('postSignin')}}">
				{{csrf_field()}}
				<img src="../images/logo.svg" alt="Canguru Hostel" class="logo">
				<div class="notifications">
					@include('errors/notifications')
				</div>
				<div class='row'>
					<div class='input-field col s12'>
						<input class='validate' type='email' name='email' id='email' />
						<label for='email'>Usuário</label>
					</div>
				</div>

				<div class='row'>
					<div class='input-field col s12'>
						<input class='validate' type='password' name='password' id='password' />
						<label for='password'>Senha</label>
					</div>
				</div>

				<center>
					<div class='row'>
						<button type='submit' name='btn_login' class='btn black-text waves-effect'>Login</button>
					</div>
				</center>
			</form>
		</div>
	</div>

	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="{{ asset('js/materialize.js') }}"></script>
</body>
</html>