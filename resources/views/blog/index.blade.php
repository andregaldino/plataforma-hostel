@extends('layouts/default')


@section('title')
{{trans('menus.blog')}}
@parent
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('css/blog.css') }}">
@endsection

@section('body')
<div class="content">
	<nav class="breadcrumb-nav grey lighten-4 z-depth-0">
		<div class="nav-wrapper">
			<div class="container">
				<div class="col s12">
					<a href="{{ route('index') }}" class="breadcrumb grey-text"><i class="material-icons">home</i></a>
					<a href="{{route('blog')}}" class="breadcrumb grey-text text-darken-3">{{trans('menus.blog')}}</a>
				</div>
			</div>
		</div>
	</nav>
	<div class="container" id="startPosts">
		<div class="category">
			<a href="#" data-activates="category" class="button-collapse right grey-text text-darken-3"><i class="material-icons small left">menu</i> <h5 class="right">{{ trans('messages.categories') }}</h5></a>
		</div>

		<ul class="side-nav side-blog" id="category">
			@forelse($categories as $category)
			<ul>
				<li class="menu-parent"><a class="grey-text text-darken-3" href="{{route('blog.category',$category->slug)}}">{{$category->translation()->name}} ({{$category->posts->count()}})</a></li>
				@forelse($category->childrens as $children)
				<li class="menu-item"><a class="grey-text text-darken-3" href="{{route('blog.category',$children->slug)}}"><i class="material-icons grey-text text-darken-3">subdirectory_arrow_right</i>{{$children->translation()->name}} ({{$children->posts->count()}})</a></li>
				@empty
				@endforelse
			</ul>
			@empty

			@endforelse
		</ul>

		<div class="row">
			@forelse($posts as $post)
			<div class="col s12 posts">			
				<div class="section">
					@if($post->pic)
					<div class="cover">
						<img src="/uploads/poi/{{$post->pic}}" alt="">
					</div>
					@endif

					<a href="{{route('blog.show',$post->slug)}}">
						<h4 class="grey-text text-darken-3">{{$post->title}}</h4>
					</a>		

					<h6 class="grey-text text-darken-1 light justify">{!!html_entity_decode(str_limit($post->body,200))!!}</h6>

					<a href="{{route('blog.show',$post->slug)}}"><h5 class="grey-text text-darken-1 light center read">{{ trans('messages.read') }}</h5></a>

					<div class="left">
						<h6 class="grey-text text-darken-1 light center"><i class="material-icons left">access_time</i>{{$post->created_at->diffForHumans()}} <a href="#">{{$post->user->name}}</a></h6>
					</div>
					<div class="right">
						<div class="chip">{{$post->category->name}}</div>
					</div>
				</div>
				<br>
				<div class="divider"></div>
			</div>
			@empty
			NENHUM POST ENCONTRADO NESSA CATEGORIA
			@endforelse
		</div>

		<div class="row">
			<div class="col s12">
				{{$posts->links()}}
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script src="{{ asset('js/blog.js') }}"></script>
@endsection