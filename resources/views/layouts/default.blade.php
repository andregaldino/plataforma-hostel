<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>
		@section('title') - Canguru Hostel
		@show
	</title>
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	<meta name="theme-color" content="#212121" />
	@yield('meta')
	<link rel="shortcut icon" href="favicon.png">

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="{{ asset('css/materialize.css') }}">
	<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/site.css') }}">

	@yield('css')
</head>
<body>
	@include('layouts/nav')
	
	<main>
		@yield('body')
	</main>

	@include('layouts/footer')

	<script src="{{ asset('js/jquery-2.1.4.js') }}"></script>
	<script src="{{ asset('js/materialize.js') }}"></script>
	<script src="{{ asset('js/site.js') }}"></script>
	<!-- Piwik -->
	<script type="text/javascript">
	  var _paq = _paq || [];
	  _paq.push(['trackPageView']);
	  _paq.push(['enableLinkTracking']);
	  (function() {
	    var u="//cluster-piwik.locaweb.com.br/";
	    _paq.push(['setTrackerUrl', u+'piwik.php']);
	    _paq.push(['setSiteId', {{env('PIWIK_SITE_ID')}}]);
	    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
	    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
	  })();
	</script>
	<noscript><p><img src="//cluster-piwik.locaweb.com.br/piwik.php?idsite={{env('PIWIK_SITE_ID')}}" style="border:0;" alt="" /></p></noscript>
	<!-- End Piwik Code -->
	@yield('script')
</body>
</html>