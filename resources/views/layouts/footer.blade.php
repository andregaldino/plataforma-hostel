<footer class="page-footer">
	<div class="container">
		<div class="row">
			<div class="col l6 s12">
				<h5 class="white-text">{{ trans('messages.contactus') }}</h5>
				<p class="grey-text text-lighten-4">
					<i class="material-icons left">place</i>
					{{$configuration->endereco}}, {{$configuration->numero}}<br>
					{{$configuration->praia}}<br>
					<span class="text-indent">
					{{$configuration->cidade}}/{{$configuration->estado}} - {{$configuration->pais}}
					</span>
				</p>
				<p class="grey-text text-lighten-4">
					<i class="material-icons left">phone</i> {{$configuration->telefone}}
				</p>
				<p class="grey-text text-lighten-4">
					<i class="material-icons left">mail</i> {{$configuration->email}}
				</p>
			</div>
			<div class="col l4 offset-l2 s12">
				<h5 class="white-text">{{ trans('messages.links') }}</h5>
				<ul>
					<li><a class="white-text" href="{{ route('booking') }}">{{ trans('menus.booknow') }}</a></li>
					<li><a class="white-text" href="{{ route('events') }}">{{ trans('messages.events') }}</a></li>
					<li><a class="white-text" href="{{ route('blog') }}">{{ trans('menus.blog') }}</a></li>
					<li><a class="white-text" href="/admin">{{ trans('menus.restrict') }}</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="grey darken-4 navbar-bottom">
		<div class="container">
			<div class="row">
				<div class="col l6 s12">
					<h5 class="white-text">{{ trans('messages.followus') }}</h5>
					<ul class="left">
						@if($configuration->facebook)
						<li><a href="https://www.facebook.com/{{$configuration->facebook}}" class="white-text" target="_blank"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></a></li>
						@endif
						@if($configuration->instagram)
						<li><a href="https://www.instagram.com/{{$configuration->instagram}}" class="white-text" target="_blank"><i class="fa fa-instagram fa-lg" aria-hidden="true"></i></a></li>
						@endif
						@if($configuration->twitter)
						<li><a href="https://twitter.com/{{$configuration->twitter}}" class="white-text" target="_blank"><i class="fa fa-twitter fa-lg" aria-hidden="true"></i></a></li>
						@endif
						@if($configuration->gplus)
						<li><a href="https://plus.google.com/{{$configuration->gplus}}" class="white-text" target="_blank"><i class="fa fa-google-plus fa-lg" aria-hidden="true"></i></a></li>
						@endif
					</ul>
				</div>
				<div class="col l4 offset-l2 s12">
					<h5 class="white-text">{{ trans('messages.chooselang') }}</h5>
					<ul>
						@foreach (Config::get('languages.locales') as $lang => $language)
						<li><a href="{{ route('lang.switch', $lang) }}" class="white-text"><img src="/images/{{ $language['icon'] }}" alt=""><span>{{ $language['language'] }}</span></a></li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="footer-copyright">
	<div class="container">
		© 2016 {{$configuration->name}}
		<span class="right">{{ trans('messages.developedby') }} <a class="grey-text text-lighten-4" href="https://github.com/andregaldino" target="_blank">André Galdino</a></span>
	</div>
</div>

</footer>