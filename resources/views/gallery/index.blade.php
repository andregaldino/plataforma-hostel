@extends('layouts/default')


@section('title')
{{trans('menus.photogallery')}}
@parent
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('css/photoswipe.css') }}">
<link rel="stylesheet" href="{{ asset('css/default-skin/default-skin.css') }}">
<link rel="stylesheet" href="{{ asset('css/gallery.css') }}">
@endsection

@section('body')
<div class="content">
	<nav class="breadcrumb-nav grey lighten-4 z-depth-0">
		<div class="nav-wrapper">
			<div class="container">
				<div class="col s12">
					<a href="{{ route('index') }}" class="breadcrumb grey-text"><i class="material-icons">home</i></a>
					<a href="{{route('galleries')}}" class="breadcrumb grey-text text-darken-3">{{trans('menus.photogallery')}}</a>
				</div>
			</div>
		</div>
	</nav>
	<div class="container">	
		<div class="row">	
			<div class="col s12">
				@forelse($galleries as $gallery)
				<h4 class="grey-text text-darken-3 center">{{$gallery->title}}</h4>
				<div class="my-gallery" itemscope itemtype="http://schema.org/ImageGallery">
					@forelse($gallery->photos as $photo)
					<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
						<a href="{{asset('uploads/gallery/'.$photo->pic)}}" itemprop="contentUrl" data-size="{{$photo->width}}x{{$photo->height}}">
							<img src="{{asset('uploads/gallery/'.$photo->pic)}}" itemprop="thumbnail" alt="{{ $photo->title}}"/>
						</a>
						<figcaption itemprop="caption description">{{$photo->title}}</figcaption>
					</figure>
					@empty
					<div class="center">
						<h4 class="grey-text text-darken-3">{{ trans('errors.gallery.noimage') }}</h4>
					</div>
					@endforelse
				</div>
				@empty
				<div class="center">
					<h4 class="grey-text text-darken-3">{{ trans('errors.gallery.nogallery') }}</h4>
				</div>
				@endforelse

				<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="pswp__bg"></div>
					<div class="pswp__scroll-wrap">
						<div class="pswp__container">
							<div class="pswp__item"></div>
							<div class="pswp__item"></div>
							<div class="pswp__item"></div>
						</div>
						<div class="pswp__ui pswp__ui--hidden">
							<div class="pswp__top-bar">
								<div class="pswp__counter"></div>
								<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
								<button class="pswp__button pswp__button--share" title="Share"></button>
								<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
								<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
								<div class="pswp__preloader">
									<div class="pswp__preloader__icn">
										<div class="pswp__preloader__cut">
											<div class="pswp__preloader__donut"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
								<div class="pswp__share-tooltip"></div> 
							</div>
							<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
							</button>
							<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
							</button>
							<div class="pswp__caption">
								<div class="pswp__caption__center"></div>
							</div>
						</div>
					</div>
				</div>
			</div>		
		</div>
	</div>
</div>
@endsection

@section('script')
<script src="{{ asset('js/photoswipe.min.js') }}"></script>
<script src="{{ asset('js/photoswipe-ui-default.min.js') }}"></script>
<script src="{{ asset('js/gallery_front.js') }}"></script>
@endsection