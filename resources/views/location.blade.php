@extends('layouts/default')

@section('title')
{{trans('menus.location')}}
@parent
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('css/location.css') }}">
<link rel="stylesheet" href="{{ asset('css/maps.css') }}">
@endsection

@section('body')
<div class="content">
	<nav class="breadcrumb-nav grey lighten-4 z-depth-0">
		<div class="nav-wrapper">
			<div class="container">
				<div class="col s12">
					<a href="{{ route('index') }}" class="breadcrumb grey-text"><i class="material-icons">home</i></a>
					<a href="{{route('location')}}" class="breadcrumb grey-text text-darken-3">{{trans('menus.location')}}</a>
				</div>
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<h4 class="grey-text text-darken-3 center">{{ trans('messages.contactus') }}</h4>
			<div class="notifications">
				@include('errors/notifications')
				@if (session('status'))
	                <div class="center">
						<div class="chip green white-text">
							{{session('status')}}
							<i class="close material-icons">close</i>
					</div>
	            @endif
			</div>
			<div class="col s12 m6 right">
				<div class="section">
					<form class="col s12" action="{{route('send.mail')}}" method="POST">
						{{ csrf_field() }}
						<div class="row">
							<div class="input-field col s4">
								<i class="material-icons prefix">account_circle</i>
								<input id="name" type="text" name="name" class="validate">
								<label for="name">Name</label>
							</div>
							<div class="input-field col s4">
								<i class="material-icons prefix">phone</i>
								<input id="phone" type="tel" name="phone" class="validate">
								<label for="phone">Telephone</label>
							</div>
							<div class="input-field col s4">
								<i class="material-icons prefix">mail</i>
								<input id="email" type="email" name="email" class="validate">
								<label for="email">E-mail</label>
							</div>
							<div class="input-field col s12">
								<i class="material-icons prefix">mode_edit</i>
								<textarea id="message" name="message" class="materialize-textarea"></textarea>
								<label for="message">Message</label>
							</div>
							<div class="center">
								<button class="waves-effect btn black-text" type="submit"><i class="material-icons right">send</i>Enviar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="col s12 m6">
				<div class="section">			
					<h6 class="grey-text text-darken-1 light">
						<ul class="social">
							@if($configuration->email)
							<li>
								<i class="fa fa-envelope fa-lg grey-text text-darken-3" aria-hidden="true"></i> {{$configuration->email}} <br>
							</li>
							@endif
							@if($configuration->telefone)
							<li>
								<i class="fa fa-phone fa-lg grey-text text-darken-3" aria-hidden="true"></i> {{$configuration->telefone}}
							</li>
							@endif
							@if($configuration->whatsapp)
							<li>
								<i class="fa fa-whatsapp fa-lg grey-text text-darken-3" aria-hidden="true"></i> {{$configuration->whatsapp}}
							</li>
							@endif
						</ul>
					</h6>
				</div>
				<div class="section">			
					<h5 class="grey-text text-darken-3">{{ trans('messages.followus') }}</h5>
					<h6 class="grey-text text-darken-1 light">
						<ul class="social">
							@if($configuration->facebook)
							<li><a href="https://www.facebook.com/{{$configuration->facebook}}" target="_blank"><i class="fa fa-facebook fa-lg grey-text text-darken-3" aria-hidden="true"></i> www.facebook.com/{{$configuration->facebook}}</a></li>
							@endif
							@if($configuration->instagram)
							<li><a href="https://www.instagram.com/{{$configuration->instagram}}" target="_blank"><i class="fa fa-instagram fa-lg grey-text text-darken-3" aria-hidden="true"></i> www.instagram.com/{{$configuration->instagram}}</a></li>
							@endif
							@if($configuration->twitter)
							<li><a href="https://twitter.com/{{$configuration->twitter}}" target="_blank"><i class="fa fa-twitter fa-lg grey-text text-darken-3" aria-hidden="true"></i> www.twitter.com/{{$configuration->twitter}}</a></li>
							@endif
							@if($configuration->gplus)
							<li><a href="https://plus.google.com/{{$configuration->gplus}}" target="_blank"><i class="fa fa-google-plus fa-lg grey-text text-darken-3" aria-hidden="true"></i>plus.google.com/{{$configuration->gplus}}</a></li>
							@endif
						</ul>
					</h6>
				</div>
				<div class="section">			
					<h5 class="grey-text text-darken-3">{{ trans('messages.address') }}</h5>
					<h6 class="grey-text text-darken-1 light">
						{{$configuration->endereco}}, {{$configuration->numero}}<br>
						{{$configuration->praia}}<br>
						{{$configuration->cidade}}/{{$configuration->estado}} - {{$configuration->pais}}
					</h6>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="map-container">
	<h4 class="grey-text text-darken-3 center">{{ trans('messages.location') }}</h4>
	<div id="map-canvas"></div>
	<a href="https://maps.google.com?saddr=Current+Location&daddr={{$configuration->name}}" target="_blank" class="btn orange darken-1 directions" title="Direções"><i class="material-icons black-text">directions</i>
	</a>
</div>
@endsection

@section('script')
<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_API')}}"></script>
<script src="{{ asset('js/MarkerWithLabel.js') }}"></script>
<script src="{{ asset('js/maps.js') }}"></script>
@endsection