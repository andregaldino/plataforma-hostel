@if ($paginator->hasPages())
    <ul class="pagination">
        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
             <li><a href="{{ $paginator->nextPageUrl() }}" class="btn-flat small black-text waves-effect" rel="prev">Older posts<i class="material-icons left">chevron_left</i></a></li>
        @endif

        {{-- Previous Page Link --}}
        @if (!$paginator->onFirstPage())
            <li><a href="{{ $paginator->previousPageUrl() }}" class="btn-flat small black-text waves-effect" rel="prev">Newer posts<i class="material-icons right">chevron_right</i></a></li>
        @endif
    </ul>
@endif
