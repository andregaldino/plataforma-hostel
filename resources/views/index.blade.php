@extends('layouts/default')

@section('title')
Home
@parent
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('css/maps.css') }}">
<link rel="stylesheet" href="{{ asset('css/index.css') }}">
@endsection

@section('body')
<header>
	<div class="parallax"><img src="images/parallax.jpg"></div>
	<div class="parallax-container">
		<form id="check" class="parallax-item center">
			<div class="row">
				<div class="col s12 m8 offset-m2 l4 offset-l4">
					<h3>{{ trans('messages.welcome') }}</h3>
				</div>
				<div class="col s12 m8 offset-m2 l4 offset-l4">
					<div class="input-field col s6">
						<div class="icon">
							<img src="/images/checkin.svg" width="30px" height="30px">
						</div>
						<input id="check_in" type="date" class="datepicker" name="check_in" placeholder="Check-in">
					</div>
					<div class="input-field col s6">
						<div class="icon">
							<img src="/images/checkin.svg" width="30px" height="30px">
						</div>
						<input id="check_out" type="date" class="datepicker" name="check_out" placeholder="Check-out">
					</div>
				</div>
			</div>
			<button class="btn-large waves-effect black-text" type="submit">{{ trans('buttons.booknow') }}</button>
		</form>
	</div>

	<div class="parallax-container parallax-about valign-wrapper">
		<div class="container">
			<div class="valign">
			<h6 class="white-text light justify">{{ trans('messages.about.message') }}</h6>
			</div>
		</div>
	</div>
</header>


@if(count($events)>0)
<div class="events">
	<div class="container">
		<h4 class="grey-text text-darken-3 center">{{ trans('messages.events') }}</h4>
		<div class="row center">
			@forelse($events as $event)
			<div class="event">
				<div class="card z-depth--1">
					<div class="card-image">
						<img src="/uploads/events/{{$event->pic}}" title="{{$event->translation()->title}}">
						<span class="card-title"><i class="material-icons left">event</i>{{$event->start_date->format('d/m/Y')}}</span>
					</div>
					<div class="card-title">
						<p>{{$event->translation()->title}}</p>
					</div>
					<div class="card-content light">
						{!!html_entity_decode(str_limit($event->translation()->description,100))!!}
					</div>
					<div class="card-action">
						<a href="{{route('events.show', $event->slug)}}">{{ trans('links.event.goto') }}</a>
					</div>
				</div>
			</div>
			@empty
			@endforelse
		</div>
	</div>
</div>
@endif

<div class="map-container grey darken-4">
	<h4 class="white-text center">{{ trans('messages.location') }}</h4>
	<div id="map-canvas"></div>
	<a href="https://maps.google.com?saddr=Current+Location&daddr={{$configuration->name}}" target="_blank" class="btn orange darken-1 directions" title="Direções"><i class="material-icons black-text">directions</i></a>
</div>
@endsection

@section('script')
<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_API')}}"></script>
<script src="{{ asset('js/MarkerWithLabel.js') }}"></script>
<script src="{{ asset('js/maps.js') }}"></script>
<script src="{{ asset('js/index.js') }}"></script>
@endsection