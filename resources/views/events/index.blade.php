@extends('layouts/default')


@section('title')
{{trans('menus.event')}}
@parent
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('css/event.css') }}">
@endsection

@section('body')
<div class="content">
	<nav class="breadcrumb-nav grey lighten-4 z-depth-0">
		<div class="nav-wrapper">
			<div class="container">
				<div class="col s12">
					<a href="{{ route('index') }}" class="breadcrumb grey-text"><i class="material-icons">home</i></a>
					<a href="{{route('events')}}" class="breadcrumb grey-text text-darken-3">{{trans('menus.event')}}</a>
				</div>
			</div>
		</div>
	</nav>
	<div class="container">
		<h4 class="grey-text text-darken-3 center">{{ trans('messages.events') }}</h4>
		<div class="row center">
			@forelse($events as $event)
			<div class="event">
				<div class="card z-depth--1">
					<div class="card-image">
						<img src="/uploads/events/{{$event->pic}}" title="{{$event->translation()->title}}">
						<span class="card-title"><i class="material-icons left">event</i>{{$event->start_date->format('d/m/Y')}}</span>
					</div>
					<div class="card-title">
						<p>{{$event->translation()->title}}</p>
					</div>
					<div class="card-content light">
						{!!html_entity_decode(str_limit($event->translation()->description,100))!!}
					</div>
					<div class="card-action">
						<a href="{{route('events.show', $event->slug)}}">{{ trans('links.event.goto') }}</a>
					</div>
				</div>
			</div>
			@empty
			@endforelse
		</div>
	</div>
</div>
@endsection

@section('script')
@endsection