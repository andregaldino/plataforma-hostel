@extends('layouts/default')

@section('title')
{{$accommodation->translation()->title}}
@parent
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('css/accommodation.css') }}">
<link rel="stylesheet" href="{{ asset('css/photoswipe.css') }}">
<link rel="stylesheet" href="{{ asset('css/default-skin/default-skin.css') }}">
<link rel="stylesheet" href="{{ asset('css/gallery.css') }}">
@endsection

@section('body')
<div class="content">
	<nav class="breadcrumb-nav grey lighten-4 z-depth-0">
		<div class="nav-wrapper">
			<div class="container">
				<div class="col s12">
					<a href="{{ route('index') }}" class="breadcrumb grey-text"><i class="material-icons">home</i></a>
					<a href="{{route('room.accommodation',$accommodation->slug)}}" class="breadcrumb grey-text text-darken-3">{{$accommodation->translation()->title}}</a>
				</div>
			</div>
		</div>
	</nav>

	<div class="container">
		<div class="row">
			<div class="col s12">
				<h4 class="grey-text text-darken-3 center">{{$accommodation->translation()->title}}</h4>
				<h5 class="grey-text text-darken-1 light center">{{$accommodation->translation()->subtitle}}</h5>
			</div>
		</div>

		<div class="row">	
			<div class="col s12">
				<div class="details z-depth--1">
					<ul class="left-align">
						<li>
							<i class="material-icons medium left">hotel</i>
							<h6 class="grey-text text-darken-1 light right">{{$accommodation->beds}}</h6>
						</li>
						<li>
							<img src="/images/{{$accommodation->gender}}.svg" title="{{$accommodation->gender}}" class="icon">
						</li>
					</ul>
					<ul class="right-align">
						<li>
							<a href="{{ route('booking') }}" class="btn-large waves-effect black-text" type="submit">{{ trans('buttons.booknow') }}</a>
						</li>
					</ul>
				</div>

				<div class="section">
					<h5 class="grey-text text-darken-3 center">{{ trans('room.detail') }}</h5>
					<h6 class="grey-text text-darken-1 light justify">{{$accommodation->translation()->description}}</h6>
				</div>
				<div class="divider"></div>

				<div class="section">
					<h5 class="grey-text text-darken-3 center">{{ trans('room.obs') }}</h5>
					<h6 class="grey-text text-darken-1 light justify">{{$accommodation->translation()->observations}}</h6>
				</div>
				<div class="divider"></div>

				<div class="section amenities center">
					<h5 class="grey-text text-darken-3 center">{{ trans('room.amenities') }}</h5>
					<ul>
						@forelse($accommodation->services as $service)
						<li>
							<img src="/images/icons/{{$service->icon}}" title="{{$service->name}}" class="icon left">
							<h6 class="grey-text text-darken-1 light right">{{$service->name}}</h6>
						</li>
						@empty
						@endforelse
					</ul>
				</div> 
				<div class="divider"></div>

				<div class="section">
					<h5 class="grey-text text-darken-3 center">{{ trans('room.bookings.title') }}</h5>
					<h6 class="grey-text text-darken-1 light">
						{{ trans('room.bookings.rates') }} <a href="#">{{ trans('room.bookings.bookingpage') }}</a>.<br>
						E-mail: <a href="mailto:{{$configuration->email}}">{{$configuration->email}}</a><br>
						{{ trans('room.bookings.phone') }}: {{$configuration->telefone}}
					</h6>
				</div> 

				
				@if($accommodation->gallery)
				<div class="divider"></div>
				<div class="section">
					<h5 class="grey-text text-darken-3 center">{{ trans('room.photos') }}</h5>				
					<div class="my-gallery" itemscope itemtype="http://schema.org/ImageGallery" class="center">
						
						@forelse($accommodation->gallery->photos as $photo)
						<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
							<a href="{{asset('uploads/gallery/'.$photo->pic)}}" itemprop="contentUrl" data-size="{{$photo->width}}x{{$photo->height}}">
								<img src="{{asset('uploads/gallery/'.$photo->pic)}}" itemprop="thumbnail" alt="{{ $photo->title}}"/>
							</a>
							<figcaption itemprop="caption description">{{$photo->title}}</figcaption>
						</figure>
						@empty
						Nenhuma imagem
						@endforelse
					</div>

					<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="pswp__bg"></div>
						<div class="pswp__scroll-wrap">
							<div class="pswp__container">
								<div class="pswp__item"></div>
								<div class="pswp__item"></div>
								<div class="pswp__item"></div>
							</div>
							<div class="pswp__ui pswp__ui--hidden">
								<div class="pswp__top-bar">
									<div class="pswp__counter"></div>
									<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
									<button class="pswp__button pswp__button--share" title="Share"></button>
									<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
									<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
									<div class="pswp__preloader">
										<div class="pswp__preloader__icn">
											<div class="pswp__preloader__cut">
												<div class="pswp__preloader__donut"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
									<div class="pswp__share-tooltip"></div> 
								</div>
								<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
								</button>
								<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
								</button>
								<div class="pswp__caption">
									<div class="pswp__caption__center"></div>
								</div>
							</div>
						</div>
					</div>
				</div> 
				@endif
			</div> 
		</div>
	</div>
</div>
@endsection

@section('script')
<script src="{{ asset('js/photoswipe.min.js') }}"></script>
<script src="{{ asset('js/photoswipe-ui-default.min.js') }}"></script>
<script src="{{ asset('js/gallery_front.js') }}"></script>
@endsection