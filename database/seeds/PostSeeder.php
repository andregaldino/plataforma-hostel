<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Post;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create('pt_BR');
    	foreach (range(1,100) as $value) {
    		Post::create([
    			'title'=>$faker->name,
    			'body'=>$faker->text($maxNbChars=10000),
    			'user_id'=>1,
    			'category_id'=>1 
    			]);
    	}
    }
}
