<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Accommodation;
use App\AccommodationTranslation;
use \Cviebrock\EloquentSluggable\Services\SlugService;

class AccommodationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$accommodation = new Accommodation;

        $languages = array();
        $accommodation_trans_pt = new AccommodationTranslation;
    	$accommodation_trans_pt->title = "DORMITÓRIO MISTO 15 CAMAS";
    	$accommodation_trans_pt->subtitle = "Nosso maior dormitório com a menor tarifa!";
    	$accommodation_trans_pt->description = "Este espaçoso e ventilado quarto no piso térreo conta com dois ventiladores de teto e grandes janelas ao seu redor, proporcionando um ambiente refrescante mesmo durante os ensolarados dias paratienses.
Conta com vestiário na área externa, 15 camas (7 beliches), tela mosquiteiro nas janelas, espaço embaixo das camas para bagagem, tomadas para todas as camas e ventiladores de teto.";
    	$accommodation_trans_pt->observations = "O valor base é fornecido para que o hóspede tenha uma expectativa de preço antes de realizar uma reserva.
Os valores podem variar para mais ou para menos de acordo com a temporada ou finais de semana.
Consulte nossa página de eventos para condições especiais para feriados.";
        $accommodation_trans_pt->language = 'pt';
        $languages[]=$accommodation_trans_pt;

        $accommodation_trans_en = new AccommodationTranslation;
    	$accommodation_trans_en->title = "DORMITORY MIXED 15 BEDS";
    	$accommodation_trans_en->subtitle = "Our biggest dorm with the lowest rate!";
    	$accommodation_trans_en->description = "This spacious and airy room on the ground floor has two ceiling fans and large windows around it, providing a refreshing environment even during sunny days in Paraty.
It has a dressing room in the external area, 15 beds (7 bunk beds), mosquito netting in the windows, space under the beds for luggage, outlets for all beds and ceiling fans";
    	$accommodation_trans_en->observations = "The base value is provided so that the guest has a price expectation before making a reservation.
Values may vary more or less depending on the season or weekends.
See our events page for special conditions for holidays.";
        $accommodation_trans_en->language = 'en';
        $languages[]=$accommodation_trans_en;

 		$faker_es = Faker::create('es_ES');
        $accommodation_trans_es = new AccommodationTranslation;
    	$accommodation_trans_es->title = "DORMITÓRIO MISTO 15 CAMAS";
    	$accommodation_trans_es->subtitle = "Nuestro mayor del dormitorio con la tasa más baja!";
    	$accommodation_trans_es->description = "Esta amplia y bien iluminada habitación en la planta baja cuenta con dos ventiladores de techo y grandes ventanas por todas partes, proporcionando un ambiente refrescante, incluso durante los días soleados paratienses.
Cuenta con vestuarios en la zona al aire libre, 15 camas literas (7), mosquitera en las ventanas, el espacio debajo de las camas para el equipaje, tomadas para todas las camas y ventilador de techo.";
    	$accommodation_trans_es->observations = "Se proporciona el valor de base para que el huésped tiene un precio esperado antes de hacer una reserva.
Los valores pueden variar más o menos de acuerdo a la temporada o fines de semana.
Ver nuestra página de eventos para condiciones especiales para las vacaciones.";
        $accommodation_trans_es->language = 'es';
        $languages[]=$accommodation_trans_es;

        $faker_en = Faker::create();
        $type = 1;
        $services = $faker_en->randomElements([1,2,3,4,5,6],3);
        $accommodation->type_id = $type;
        $accommodation->title = $accommodation_trans_en->title;
        $accommodation->beds = 12;
        $accommodation->gender = "mixed";
        $accommodation->save();

        $accommodation->services()->sync($services);
        $accommodation->translations()->saveMany($languages);
    }
}
