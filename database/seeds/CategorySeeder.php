<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
        	'name'=>'Cidade',
        	]);
        Category::create([
        	'name'=>'Festas',
        	]);
        Category::create([
        	'name'=>'Lazer',
        	]);
        Category::create([
        	'name'=>'Comida',
        	]);
        Category::create([
            'name'=>'Japonesa',
            'parent_id'=>3,
            ]);
        Category::create([
            'name'=>'Virada Cultural',
            'parent_id'=>'1'
            ]);
    }
}
