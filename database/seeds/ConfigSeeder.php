<?php

use Illuminate\Database\Seeder;
use App\Configuration;
class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Configuration::create([
            'name'=>'Canguru Hostel',
            'rss'=>'http://www.paratyonline.com/jornal/category/eventos-paraty/feed/',
            'facebook'=>'CanguruHostel',
            'instagram'=>'CanguruHostel',
            'twitter'=>'CanguruHostel',
            'gplus'=>'102129971643689498294',
            'cidade'=>'Paraty',
            'estado'=>'RJ',
            'pais'=>'Brasil',
            'endereco'=>'Av. Vera Cruz',
            'numero'=>'46',
            'bairro'=>'Jabaquara',
            'praia'=>'Praia do Jabaquara',
            'telefone'=>'+55 24 3371-1477 ',
            'email'=>'contato@canguruhostel.com.br',
            'atendimento'=>'Recepção 24h!',
            'cep'=>'23970-000'
            ]);
    }
}
