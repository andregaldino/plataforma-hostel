<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //criando o usuario admin
		$admin = Sentinel::registerAndActivate(array(
			'email'       => 'admin@hostel.com',
			'password'    => "0Cc65XJd73",
			'name'  => 'André Galdino',
		));

		//criando os grupos
		$adminRole = Sentinel::getRoleRepository()->createModel()->create([
			'name' => 'Admin',
			'slug' => 'admin',
			'permissions' => array('admin' => 1),
		]);

		Sentinel::getRoleRepository()->createModel()->create([
			'name'  => 'Usuario',
			'slug'  => 'usuario',
		]);

		$userRole = Sentinel::getRoleRepository()->createModel()->create([
			'name'  => 'Proprietario',
			'slug'  => 'proprietario',
		]);


		//adicionando o grupo admin ao user admin
		$admin->roles()->attach($adminRole);

		$this->command->info('Admin User created with username admin@admin.com and password ');
    }
}
