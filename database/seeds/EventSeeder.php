<?php

use Illuminate\Database\Seeder;
use App\Event;
use Carbon\Carbon;
class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        	$event = new Event;
        	$event->title = "Festival da Pinga no Paraty 33 tem Banda Rolls Rock + DJ Djahra na sexta 12 e sábado 13/08!!";
        	$event->description = "Astro do Programa Raul Gil do SBT e líder da Banda Rolls Rock, Edu Costa se apresenta no palco da casa mais badalada de Paraty!!

Almoço, jantar e balada com a melhor música ao vivo na casa top do litoral!! Confira a programação especial do 33 para a XXXIV edição do Festival da Cachaça, Cultura e Sabores de Paraty.


Programação sexta 12 e sábado 13 de agosto de 2016:

– The Party com Banda Rolls Rock: com muita pegada e um repertório eclético, este grupo agrada todas as gerações, deixando as pessoas entretidas e satisfeitas com suas apresentações, fazendo covers de AC-DC, Iron Maiden, Pink Floyd, Deep Purple, Bon Jovi, Creedence, Guns ‘N Roses e INXS, entre outros.

Edu Costa e sua guitarra, deixaram os jurados do Quem Sabe Canta 2016 boquiabertos!! Se liga no vídeo do You Tube

– DJ Djahra nos intervalos dos recitais e a noite toda: o DJ Residente apresenta um set-list sensacional, variado, com os maiores hits do cenário eletrônico mundial!";
        	$event->start_date = Carbon::createFromDate(2017, 1, 12);
        	$event->pic = '1.jpg';

        	$event->user()->associate(1);
        	$event->save();


            $event1 = new Event;
            $event1->title = "Paraty 33 abre o verão com maratona de shows espetaculares! Confira a programação!";
            $event1->description = "Agenda Dezembro: 23 e 24 Nilo Mariano & Banda – 25 Banda Rolls Rock – 26 Banda Panela – 27 e 28 Banda Rolls Rock + DJ Roger (convidado) – 29 Nilo Mariano & Banda – 30 e 31 Banda Rock Collection – Todas as noites Resident DJ Djahra!!

E mais: Ceia de Natal com música ao vivo + Banda ao Vivo+ DJ e Ceia de Réveillon com música ao vivo + Balada da Virada com Banda! Saiba mais AQUI

A casa mais badalada de Paraty apresenta os melhores event1os para abrir o verão, despedir o ano e receber 2017 com força total!!

Faça sua reserva agora, ligue (24) 3371 7311 / 7308 ou envie um email a paraty33@paraty33.com.br O Paraty 33 fica na Rua Maria Jácome de Melo, 357 (Rua da Lapa), Centro Histórico.";
            $event1->start_date = Carbon::createFromDate(2016, 5, 12);
            $event1->pic = '1.jpg';

            $event1->user()->associate(1);
            $event1->save();


            $event2 = new Event;
            $event2->title = "Weekend Coupê do Festival da Pinga tem receitas flambadas na cachaça de Paraty! Menu completo R$ 38,40";
            $event2->description = "De quinta a domingo, entrada + prato principal R$ 38,40! A promoção gourmet espera você na esquina em frente da igreja da Matriz durante o XXXIV Festival da Cachaça, Cultura e Sabores de Paraty!


MENU – DE QUINTA A DOMINGO

Entrada:

Involtine de zukini recheado com mussarela de búfala e tomate seco,
gratinado com pomodoro e parmesão

Opção 1:

Iscas de baby beef flambadas na cachaça de Paraty
com espaguete ao pomodoro, basílico e toscana picante

Opção 2:

Lascas de salmão flambadas na cachaça de Paraty
com penne ao creme de natas e camarões

VEJA OS CARDÁPIOS DE COMIDAS E BEBIDAS COMPLETOS AQUI

– Aberto todos os dias para almoço e jantar
– Email: info@casacoupe.com.br
– Site: www.casacoupe.com.br

Faça sua reserva pelo telefone (24) 3371 6008. A Casa Coupê fica na esquina em frente da Igreja da Matriz.";
            $event2->start_date = Carbon::createFromDate(2016, 12, 27);
            $event2->pic = '1.jpg';

            $event2->user()->associate(1);
            $event2->save();
    }
}
