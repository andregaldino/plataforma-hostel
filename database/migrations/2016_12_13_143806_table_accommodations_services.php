<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableAccommodationsServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accommodation_service',function(Blueprint $table){
            $table->increments('id');
            $table->integer('accommodation_id')->unsigned();
            $table->integer('service_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('accommodation_id')->references('id')->on('accommodations');
            $table->foreign('service_id')->references('id')->on('services');
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accommodation_service');
    }
}
