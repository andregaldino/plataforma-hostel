<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableTermsTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('term_translations',function(Blueprint $table){
            $table->increments('id');
            $table->string('language');
            $table->string('title');
            $table->text('description');
            $table->integer('term_id')->unsigned();
            $table->foreign('term_id')
                    ->references('id')
                    ->on('terms')
                    ->onDelete('cascade');

            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('term_translations');
    }
}
