<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeTranslation extends Model
{
	protected $table = 'type_translations';
    protected $fillable = ['language', 'name', 'description','type_id'];

    public function type()
    {
        return $this->belongsTo('App\Type');
    }
}
