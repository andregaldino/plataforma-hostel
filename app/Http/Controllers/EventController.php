<?php

namespace App\Http\Controllers;

use App\Http\Requests\EventRequest;
use App\Event;
use App\EventTranslation;
use Carbon\Carbon;
use Sentinel;
use App\Gallery;
use File;

class EventController extends HostelController
{
	public function index()
	{
		if (!Sentinel::getUser()->hasAccess('event.index')) {
            return redirect()->route('dash.index')->withErrors("Você não tem acesso a essa funcionalidade");
        }
    	$events = Event::orderBy('created_at', 'desc')->paginate(10);
		return view('admin.event.index', compact('events'));
	}

	public function create()
	{
		if (!Sentinel::getUser()->hasAccess('event.create')) {
            return redirect()->route('dash.index')->withErrors("Você não tem acesso a essa funcionalidade");
        }
		$galleries = Gallery::all();
		return view('admin.event.create',compact('galleries'));
	}

	public function store(EventRequest $request)
	{
		try {
			if (!Sentinel::getUser()->hasAccess('event.create')) {
                throw new Exception("Você não tem acesso a essa funcionalidade", 1);
            }
			$event = new Event;
			$event->title = $request->get('english_title');
			$event->start_date = Carbon::createFromFormat('d/m/Y', $request->get('start_date'));
			$event->user()->associate(Sentinel::getUser());
			
			if ($file = $request->file('pic')) {
    			$fileName = $file->getClientOriginalName();
    			$extension = $file->getClientOriginalExtension() ?: 'png';
    			$folderName = '/uploads/events/';
    			$destinationPath = public_path() . $folderName;
    			$safeName = md5($event->title) . '.' . $extension;
    			$file->move($destinationPath, $safeName);
    			$event->pic = $safeName;
    		}

    		$languages = array();
          	$trans_pt = new EventTranslation;
            $trans_pt->title = $request->get('portuguese_title');
            $trans_pt->description = $request->get('portuguese_description');
            $trans_pt->language = 'pt';
            $languages[]=$trans_pt;

            $trans_en = new EventTranslation;
            $trans_en->title = $request->get('english_title');
            $trans_en->description = $request->get('english_description');
            $trans_en->language = 'en';
            $languages[]=$trans_en;

            $trans_es = new EventTranslation;
            $trans_es->title = $request->get('spanish_title');
            $trans_es->description = $request->get('spanish_description');
            $trans_es->language = 'es';
            $languages[]=$trans_es;


    		if($request->has('gallery_id')){
				$gallery = Gallery::findOrFail($request->get('gallery_id'));
				$event->gallery()->associate($gallery);
			}
			$event->save();
			$event->translations()->saveMany($languages);
    		
    		return redirect()->route('dash.event.index');
		} catch (Exception $e) {
			return redirect()->back()->withErrors($e->getMessage());
			
		}
	}

	public function edit(Event $event)
	{
		if (!Sentinel::getUser()->hasAccess('event.edit')) {
            return redirect()->route('dash.index')->withErrors("Você não tem acesso a essa funcionalidade");
        }
		$galleries = Gallery::all();
		return view('admin.event.edit',compact('event','galleries'));
	}

	public function update(EventRequest $request,Event $event)
	{
		try {

			if (!Sentinel::getUser()->hasAccess('event.edit')) {
                throw new Exception("Você não tem acesso a essa funcionalidade", 1);
            }

			$event->title = $request->get('english_title');
			$event->start_date = Carbon::createFromFormat('d/m/Y', $request->get('start_date'));
			if ($file = $request->file('pic')) {
    			$fileName = $file->getClientOriginalName();
    			$extension = $file->getClientOriginalExtension() ?: 'png';
    			$folderName = '/uploads/events/';
    			$destinationPath = public_path() . $folderName;
    			$safeName = md5($event->title) . '.' . $extension;
    			$file->move($destinationPath, $safeName);


                if (File::exists(public_path() . $folderName . $event->pic)) {
                    File::delete(public_path() . $folderName . $event->pic);
                }

    			$event->pic = $safeName;
    		}
    		if($request->has('gallery_id')){
				$gallery = Gallery::findOrFail($request->get('gallery_id'));
				$event->gallery()->associate($gallery);
			}

			$languages = array();
          	$trans_pt = new EventTranslation;
            $trans_pt->title = $request->get('portuguese_title');
            $trans_pt->description = $request->get('portuguese_description');
            $trans_pt->language = 'pt';
            $languages[]=$trans_pt;

            $trans_en = new EventTranslation;
            $trans_en->title = $request->get('english_title');
            $trans_en->description = $request->get('english_description');
            $trans_en->language = 'en';
            $languages[]=$trans_en;

            $trans_es = new EventTranslation;
            $trans_es->title = $request->get('spanish_title');
            $trans_es->description = $request->get('spanish_description');
            $trans_es->language = 'es';
            $languages[]=$trans_es;

            $event->save();
			$event->translations()->saveMany($languages);
    		return redirect()->route('dash.event.index');
		} catch (Exception $e) {
			return redirect()->back()->withErrors($e->getMessage());
			
		}
	}

	public function destroy(Event $event)
	{
		try {
			if (!Sentinel::getUser()->hasAccess('event.delete')) {
                throw new Exception("Você não tem acesso a essa funcionalidade", 1);
            }
			$event->delete();
			return redirect()->route('dash.event.index');
		} catch (Exception $e) {
			return redirect()->back()->withErrors($e->getMessage());
		}
	}
}
