<?php

namespace App\Http\Controllers;

use Feeds;
use App\Post;
use App\Event;
use App\Category;
use App\Type;
use App\Accommodation;
use Illuminate\Http\Request;
use Session;
use App\Gallery;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactUs;
use App\Http\Requests\ContactRequest;
use App\Term;

class FrontendController extends HostelController
{
	public function index()
	{
		$events = Event::take(4)->orderBy('start_date','desc')->get();
		return view('index',compact('events'));
	}

	public function about()
	{
		return view('about');
	}

	public function terms()
	{
		$terms = Term::all();
		return view('terms',compact('terms'));
	}

	public function feeds($size)
	{
		
		$feed = Feeds::make([$this->hostel->rss], $size, true); // if RSS Feed has invalid mime types, force to read
		$data = array(
			'title'     => $feed->get_title(),
			'permalink' => $feed->get_permalink(),
			'items'     => $feed->get_items(),
			);
		return $data['items'];
	}

	public function galleries()
	{
		$galleries = Gallery::all();
		return view('gallery.index',compact('galleries'));
	}

	public function blog()
	{

		$categories = Category::where('parent_id','=',null)->get();
		$posts = Post::orderBy('created_at', 'desc')->simplePaginate(5);
		return view('blog.index',compact('posts','categories'));
	}

	public function blogCategory($slug)
	{
		try{	
			$category = Category::findBySlugOrFail($slug);
			$posts = Post::where('category_id','=',$category->id)->orderBy('created_at', 'desc')->simplePaginate(5);
			$categories = Category::where('parent_id','=',null)->get();
		}catch(Exception $e){
			return redirect()->route('blog');
		}
		return view('blog.index',compact('posts','categories'));
	}

	public function post($slug)
	{
		try {
			$post = Post::findBySlugOrFail($slug);
		} catch (Exception $e) {
			return redirect()->route('index');
		}
		return view('blog.post',compact('post'));
	}

	public function news()
	{
		$feeds = $this->feeds(10);
		return view('news',compact('feeds'));
	}

	public function events()
	{
		$events = Event::orderBy('start_date','desc')->get();
		return view('events.index',compact('events'));
	}

	public function eventSlug($slug)
	{
		try {
			$event = Event::findBySlugOrFail($slug);
		} catch (Exception $e) {
			return redirect()->route('index');
		}
		return view('events.event',compact('event'));
	}

	public function typeRoom(Type $type)
	{
		return view('typeroom',compact('type'));
	}

	public function accommodation($slug)
	{
		try {
			$accommodation = Accommodation::findBySlugOrFail($slug);	
		} catch (Exception $e) {
			return redirect()->route('index');
		}
		
		return view('accommodations.room',compact('accommodation'));
	}

	public function booking(Request $request)
	{
		$lang = Session::get('applocale');
		if($lang=='es'){
			$lang = 'en';
		}else if($lang == 'pt'){
			$lang = 'pt-br';
		}else{
			$lang = 'en';
		}
		$urlFrame = "https://admin.hqbeds.com.br/".$lang."/hqb/DLjZPB0gko/availability";
		if($request->has('start_date') && $request->has('end_date')){
			$urlDate = "?arrival=".$request->get('start_date')."&departure=".$request->get('end_date')."&coupon_code=";
			$urlFrame .= $urlDate;
		}
		return view('booking',compact('urlFrame'));
	}

	public function location()
	{
		return view('location');
	}

	public function contact()
	{
		return view('contact');
	}

	public function postContact(ContactRequest $request)
	{
		try {
			Mail::to($this->hostel->email)->send(new ContactUs($request->only(['email','name','phone','message'])));
			return redirect()->back()->with('status',trans('messages.thanks'));
		} catch (Exception $e) {
			return redirect()->back();
		}
	}
}
