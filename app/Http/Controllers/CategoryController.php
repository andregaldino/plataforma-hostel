<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Category;
use App\CategoryTranslation;

class CategoryController extends HostelController
{
    public function index()
    {
    	$categories = Category::orderBy('created_at', 'desc')->paginate(10);
    	return view('admin.category.index',compact('categories'));
    }

    public function create()
    {
    	return view('admin.category.create');
    }

    public function store(CategoryRequest $request)	
    {
    	try {
    		$category = new Category;
            $languages = array();
            $trans_pt = new CategoryTranslation;
            $trans_pt->name = $request->get('portuguese_name');
            $trans_pt->language = 'pt';
            $languages[]=$trans_pt;

            $trans_en = new CategoryTranslation;
            $trans_en->name = $request->get('english_name');
            $trans_en->language = 'en';
            $languages[]=$trans_en;

            $trans_es = new CategoryTranslation;
            $trans_es->name = $request->get('spanish_name');
            $trans_es->language = 'es';
            $languages[]=$trans_es;

    		$category->name = $request->get('english_name');
    		if ($request->has('parent_category')) {
    			$parent = Category::findOrFail($request->get('parent_category'));
    			$category->parent()->associate($parent);
    		}
            $category->save();
            $category->translations()->saveMany($languages);
    		return redirect()->route('dash.category.index');
    	} catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
    	}
    }

    public function edit(Category $category)
    {
    	return view('admin.category.edit',compact('category'));
    }

    public function update(CategoryRequest $request,Category $category)
    {
    	try {
    		$languages = array();
            $trans_pt = new CategoryTranslation;
            $trans_pt->name = $request->get('portuguese_name');
            $trans_pt->language = 'pt';
            $languages[]=$trans_pt;

            $trans_en = new CategoryTranslation;
            $trans_en->name = $request->get('english_name');
            $trans_en->language = 'en';
            $languages[]=$trans_en;

            $trans_es = new CategoryTranslation;
            $trans_es->name = $request->get('spanish_name');
            $trans_es->language = 'es';
            $languages[]=$trans_es;

            $category->name = $request->get('english_name');

    		if ($request->has('parent_category')) {
    			$parent = Category::findOrFail($request->get('parent_category'));
    			$category->parent()->associate($parent);
    		}

            $category->save();
            $category->translations()->saveMany($languages);
    		return redirect()->back();
    	} catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
    	}
    }

    public function show(Category $category)
    {
        return view('admin.category.show',compact('category'));
    }

    public function destroy(Category $category)
    {
        try {
            $category->delete();
            return redirect()->back();
        } catch (Exception $e) {
           return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
