<?php

namespace App\Http\Controllers;

use App\Http\Requests\StaffRequest;
use App\Staff;

class StaffController extends Controller
{
    public function index()
    {
    	$staffs = Staff::orderBy('created_at', 'desc')->paginate(10);
    	return view('admin.staff.index',compact('staffs'));
    }

    public function create()
    {
    	return view('admin.staff.create');
    }

    public function store(StaffRequest $request)
    {
    	try {
    		$staff = new Staff;
            $staff->name = $request->name;
            $staff->description = $request->description;
            if ($file = $request->file('pic')) {
    			$fileName = $file->getClientOriginalName();
    			$extension = $file->getClientOriginalExtension() ?: 'png';
    			$folderName = '/uploads/staffs/';
    			$destinationPath = public_path() . $folderName;
    			$safeName = md5($staff->title) . '.' . $extension;
    			$file->move($destinationPath, $safeName);
    			$staff->pic = $safeName;
    		}


            $staff->save(); 
	    	return redirect()->route('dash.staff.index');
    	} catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());	
    	}
    	
    	
    }

    public function edit(Staff $staff)
    {
    	return view('admin.staff.edit',compact('staff'));
    }

    public function update(StaffRequest $request, Staff $staff)
    {
    	try {
             $staff->name = $request->name;
            $staff->description = $request->description;

            if ($file = $request->file('pic')) {
    			$fileName = $file->getClientOriginalName();
    			$extension = $file->getClientOriginalExtension() ?: 'png';
    			$folderName = '/uploads/staffs/';
    			$destinationPath = public_path() . $folderName;
    			$safeName = md5($staff->title) . '.' . $extension;
    			$file->move($destinationPath, $safeName);
    			$staff->pic = $safeName;
    		}

            $staff->save(); 
	    	return redirect()->route('dash.staff.index');
    	} catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());	
    	}
    }

    public function show(Staff $staff)
    {
        return view('admin.staff.show',compact('staff'));
    }

    public function destroy(Staff $staff)
    {
        try {
            $staff->delete();
            return redirect()->route('dash.staff.index');
        } catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());  
        }
    }
}
