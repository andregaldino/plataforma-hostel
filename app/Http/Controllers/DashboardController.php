<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends HostelController
{
    public function index()
    {
    	return view('admin.index');
    }
}
