<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\HostelController;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends HostelController
{
    /**
     * Account sign in.
     *
     * @return View
     */
    public function getSignup()
    {
        return View('user.register');
    }

    /**
     * Account sign up form processing.
     *
     * @return Redirect
     */
    public function postSignup(UserRequest $request)
    {
        $error = "";
        $dados = $request->only('username','name', 'gender','email','password');
        $dados['dob']=Carbon::createFromFormat('d/m/Y', $request->get('dob'));
        try {
            $user = Sentinel::registerAndActivate($dados);

            $role = Sentinel::findRoleBySlug('user');
            if ($role) {
                $role->users()->attach($user);
            }

            Sentinel::login($user, false);

            Toastr::success('Usuario cadastrado com sucesso');
            return redirect("/");

        } catch (UserExistsException $e) {
            $error = $e->getMessage();
        } catch (Exception $e2){
            $error = $e2->getMessage();
        }
        Toastr::error($error);
        return redirect()->back();
    } 
}
