<?php

namespace App\Http\Controllers;

use App\Http\Requests\ConfigRequest;
use App\Configuration;

class ConfigurationController extends HostelController
{
	public function show()
	{
		return view('admin.configuration');
	}

	public function store(ConfigRequest $request)
    {
    	try {
    		$config = Configuration::findOrFail(1);
	    	$config->name = $request->get('name');
	    	$config->rss = $request->get('rss');
	    	$config->facebook = $request->get('facebook');
	    	$config->instagram = $request->get('instagram');
	    	$config->twitter = $request->get('twitter');
	    	$config->whatsapp = $request->get('whatsapp');
	    	$config->gplus = $request->get('gplus');
	    	$config->cep = $request->get('zip');
	    	$config->estado = $request->get('state');
	    	$config->pais = $request->get('country');
	    	$config->cidade = $request->get('city');
	    	$config->endereco = $request->get('address');
	    	$config->numero = $request->get('number');
	    	$config->bairro = $request->get('neighborhood');
	    	$config->praia = $request->get('beach');
	    	$config->telefone = $request->get('phone');
	    	$config->email = $request->get('email');

	    	$config->save();	
	    	return redirect()->route('dash.config');
    	} catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());	
    	}	
    }
}
