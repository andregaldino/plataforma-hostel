<?php

namespace App\Http\Controllers;

use Config;
use Cookie;
use Session;

class LanguageController extends HostelController
{
    public function switchLang($lang)
    {
        if (array_key_exists($lang, Config::get('languages.locales'))) {
			Cookie::queue('applocale', $lang, 3600);
            Session::set('applocale', $lang);
        }
        return redirect()->back();
    }
}
