<?php

namespace App\Http\Controllers;

use App\Http\Requests\AccommodationRequest;
use App\Accommodation;
use App\AccommodationTranslation;
use App\Type;
use App\Service;
use App\Gallery;
use Illuminate\Support\Facades\App;
use \Cviebrock\EloquentSluggable\Services\SlugService;
use Sentinel;

class AccommodationController extends Controller
{
    public function index()
    {
        if (!Sentinel::getUser()->hasAccess('accommodation.index')) {
            return redirect()->route('dash.index')->withErrors("Você não tem acesso a essa funcionalidade");
        }
    	$accommodations = Accommodation::orderBy('created_at','desc')->paginate(10);
    	return view('admin.accommodation.index',compact('accommodations'));
    }

    public function create()
    {
        if (!Sentinel::getUser()->hasAccess('accommodation.create')) {
            return redirect()->route('dash.index')->withErrors("Você não tem acesso a essa funcionalidade");
        }
        $types = Type::all();
    	$services = Service::all();
        $galleries = Gallery::all();
    	return view('admin.accommodation.create',compact('types', 'services','galleries'));
    }

    public function store(AccommodationRequest $request)
    {
    	try {
            if (!Sentinel::getUser()->hasAccess('accommodation.create')) {
                throw new Exception("Você não tem acesso a essa funcionalidade", 1);
            }
    		$accommodation = new Accommodation;	    	
	    	$type = Type::findOrFail($request->get('type_id'));
	    	$accommodation->type()->associate($type);

            if ($request->has('gallery_id')) {
                $gallery = Gallery::findOrFail($request->get('gallery_id'));
                $accommodation->gallery()->associate($gallery);
            }

            $languages = array();
            
            $trans_pt = new AccommodationTranslation;
            $trans_pt->title = $request->get('portuguese_title');
            $trans_pt->subtitle = $request->get('portuguese_subtitle');
            $trans_pt->description = $request->get('portuguese_description');
            $trans_pt->observations = $request->get('portuguese_observations');
            $trans_pt->language = 'pt';
            $languages[]=$trans_pt;

            $trans_en = new AccommodationTranslation;
            $trans_en->title = $request->get('english_title');
            $trans_en->subtitle = $request->get('english_subtitle');
            $trans_en->description = $request->get('english_description');
            $trans_en->observations = $request->get('english_observations');
            $trans_en->language = 'en';
            $languages[]=$trans_en;

            $trans_es = new AccommodationTranslation;
            $trans_es->title = $request->get('spanish_title');
            $trans_es->subtitle = $request->get('spanish_subtitle');
            $trans_es->description = $request->get('spanish_description');
            $trans_es->observations = $request->get('spanish_observations');
            $trans_es->language = 'es';
            $languages[]=$trans_es;
            

            $accommodation->title = $trans_en->title;
            $accommodation->beds = $request->beds;
            $accommodation->gender = $request->gender;
            $accommodation->save(); 
            $accommodation->translations()->saveMany($languages); 
            $services = $request->get('services');
            if (count($services) > 0) {
                $accommodation->services()->sync($services);
            }
	    	return redirect()->route('dash.accommodation.index');
    	} catch (Exception $e) {
    		return redirect()->back()->withErrors($e->getMessage());
    	}
    	
    	
    }

    public function edit(Accommodation $accommodation)
    {
        if (!Sentinel::getUser()->hasAccess('accommodation.edit')) {
            return redirect()->route('dash.index')->withErrors("Você não tem acesso a essa funcionalidade");
        }
    	$types = Type::all();
        $services = Service::all();
        $galleries = Gallery::all();
    	return view('admin.accommodation.edit',compact('types','accommodation','services','galleries'));
    }

    public function update(AccommodationRequest $request, Accommodation $accommodation)
    {
    	try {
            if (!Sentinel::getUser()->hasAccess('accommodation.edit')) {
                throw new Exception("Você não tem acesso a essa funcionalidade", 1);
            }
            if ($request->has('gallery_id')) {
                $gallery = Gallery::findOrFail($request->get('gallery_id'));
                $accommodation->gallery()->associate($gallery);
            }

            $trans_pt = AccommodationTranslation::findOrFail($request->get('pt_accommodation_translation_id'));
            $trans_pt->title = $request->get('portuguese_title');
            $trans_pt->subtitle = $request->get('portuguese_subtitle');
            $trans_pt->description = $request->get('portuguese_description');
            $trans_pt->observations = $request->get('portuguese_observations');
            $trans_pt->save();

            $trans_en = AccommodationTranslation::findOrFail( $request->get('en_accommodation_translation_id'));
            $trans_en->title = $request->get('english_title');
            $trans_en->subtitle = $request->get('english_subtitle');
            $trans_en->description = $request->get('english_description');
            $trans_en->observations = $request->get('english_observations');
            $trans_en->save();

            $trans_es = AccommodationTranslation::findOrFail($request->get('es_accommodation_translation_id'));
            $trans_es->title = $request->get('spanish_title');
            $trans_es->subtitle = $request->get('spanish_subtitle');
            $trans_es->description = $request->get('spanish_description');
            $trans_es->observations = $request->get('spanish_observations');
            $trans_es->save();
	    	
	    	$type = Type::findOrFail($request->get('type_id'));
	    	$accommodation->type()->associate($type);

            $services = $request->get('services');
            if (count($services) > 0) {
                $accommodation->services()->sync($services);
            }else{
                $accommodation->services()->detach();
            }

            $accommodation->beds = $request->beds;
            $accommodation->gender = $request->gender;
            $accommodation->save();
	    	return redirect()->route('dash.accommodation.index');
    	} catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());  
    	}
    }

    public function show(Accommodation $accommodation)
    {
        return view('admin.accommodation.show',compact('accommodation'));
    }

    public function destroy(Accommodation $accommodation)
    {
        try {
            if (!Sentinel::getUser()->hasAccess('accommodation.delete')) {
                throw new Exception("Você não tem acesso a essa funcionalidade", 1);
            }
            $accommodation->delete();
            return redirect()->route('dash.accommodation.index');
        } catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
