<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Icon;
use App\Http\Requests\IconRequest;
use File;

class IconController extends Controller
{

	protected $folderName;

	public function __construct()
	{
		$this->folderName = '/images/icons/';
	}

    public function index()
	{
    	$icons = Icon::orderBy('created_at', 'desc')->paginate(10);
		return view('admin.icon.index', compact('icons'));
	}

	public function create()
	{
		return view('admin.icon.create');
	}

	public function store(IconRequest $request)
	{
		try {
			$icon = new Icon;
			$icon->name = $request->get('name');
			
			if ($file = $request->file('path')) {
    			$fileName = $file->getClientOriginalName();
    			$extension = $file->getClientOriginalExtension() ?: 'svg';
    			$destinationPath = public_path() . $this->folderName;
    			$safeName = str_slug($icon->name) . '.' . $extension;
    			$file->move($destinationPath, $safeName);
    			$icon->path = $safeName;
    		}
			$icon->save();
    		return redirect()->route('dash.icon.index');
		} catch (Exception $e) {
			return redirect()->back()->withErrors($e->getMessage());
			
		}
	}

	public function edit(Icon $icon)
	{
		return view('admin.icon.edit',compact('icon'));
	}

	public function update(IconRequest $request,Icon $icon)
	{
		try {

			$icon->name = $request->get('name');
			if($request->has('path')){
				if ($file = $request->file('path')) {
	    			$fileName = $file->getClientOriginalName();
	    			$extension = $file->getClientOriginalExtension() ?: 'svg';
	    			$destinationPath = public_path() . $this->folderName;
	    			$safeName = str_slug($icon->name) . '.' . $extension;
	    			$file->move($destinationPath, $safeName);
	    			if (File::exists(public_path() . $folderName . $icon->path)) {
	                    File::delete(public_path() . $folderName . $icon->path);
	                }
	                $icon->path = $safeName;
	    		}	
			}
			

            $icon->save();
			
    		return redirect()->route('dash.icon.index');
		} catch (Exception $e) {
			return redirect()->back()->withErrors($e->getMessage());
			
		}
	}

	public function destroy(Icon $icon)
	{
		try {
			if (File::exists(public_path() . $this->folderName . $icon->path)) {
                File::delete(public_path() . $this->folderName . $icon->path);
                $icon->delete();
            }
			return redirect()->route('dash.icon.index');
		} catch (Exception $e) {
			return redirect()->back()->withErrors($e->getMessage());
		}
	}
}
