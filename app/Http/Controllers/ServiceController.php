<?php

namespace App\Http\Controllers;

use App\Http\Requests\ServiceRequest;
use App\Service;
use App\ServiceTranslation;
use App\Icon;

class ServiceController extends Controller
{
    public function index()
    {
    	$services = Service::orderBy('created_at', 'desc')->paginate(10);
    	return view('admin.service.index',compact('services'));
    }

    public function create()
    {
        $files = Icon::all();
    	return view('admin.service.create',compact('files'));
    }

    public function store(ServiceRequest $request)
    {
    	try {
    		$service = new Service;
	    	$service->icon = $request->get('icon');

            $languages = array();
            $trans_pt = new ServiceTranslation;
            $trans_pt->name = $request->get('portuguese_name');
            $trans_pt->language = 'pt';
            $languages[]=$trans_pt;

            $trans_en = new ServiceTranslation;
            $trans_en->name = $request->get('english_name');
            $trans_en->language = 'en';
            $languages[]=$trans_en;

            $trans_es = new ServiceTranslation;
            $trans_es->name = $request->get('spanish_name');
            $trans_es->language = 'es';
            $languages[]=$trans_es;
            
            $service->save();   
            $service->translations()->saveMany($languages);
	    	
	    	return redirect()->route('dash.service.index');
    	} catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());	
    	}
    	
    	
    }

    public function edit(Service $service)
    {
        $files = Icon::all();
    	return view('admin.service.edit',compact('service','files'));
    }

    public function update(ServiceRequest $request, Service $service)
    {
    	try {
	    	$service->icon = $request->get('icon');


            $languages = array();
            $trans_pt = new ServiceTranslation;
            $trans_pt->name = $request->get('portuguese_name');
            $trans_pt->language = 'pt';
            $languages[]=$trans_pt;

            $trans_en = new ServiceTranslation;
            $trans_en->name = $request->get('english_name');
            $trans_en->language = 'en';
            $languages[]=$trans_en;

            $trans_es = new ServiceTranslation;
            $trans_es->name = $request->get('spanish_name');
            $trans_es->language = 'es';
            $languages[]=$trans_es;

            $service->save();
            $service->translations()->saveMany($languages);
	    		
	    	return redirect()->route('dash.service.index');
    	} catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());	
    	}
    }

    public function show(Service $service)
    {
        return view('admin.service.show',compact('service'));
    }


    public function destroy(Service $service)
    {
        try {
            $service->delete();
            return redirect()->route('dash.service.index');
        } catch (Exception $e) {
            return redirect()->back()->withErrors($e->getMessage());  
        }
    }
}
