<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\UserRequest;
use Sentinel;
use Exception;

class UserController extends HostelController
{
	public function index()
	{
		$users = User::paginate(10);
		return View('admin.user.index',compact('users'));
	}

	public function create()
	{
		$roles = Sentinel::getRoleRepository()->all();
		return View('admin.user.create', compact('roles'));
	}


	public function edit(User $user)
	{
		$roles = Sentinel::getRoleRepository()->all();
		return View('admin.user.edit',compact('user', 'roles'));
	}

	public function store(UserRequest $request)
	{
        try {
	        $dados = $request->only('name','email','password');
            $user = Sentinel::registerAndActivate($dados);

            $role = Sentinel::findRoleById($request->get('roles'));
            if ($role) {
            	$user->roles()->detach();
                $role->users()->attach($user);
            }

            return redirect()->route('dash.user.index');

        } catch (Exception $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }

        
	}


	public function update(UserRequest $request,$user)
	{
		try {
			$credenciais = [];
			//verificando se existe o campo password
			if ($request->has('password')) {
				//para trocar o password
				$credenciais['password'] = $request->get('password');
			}
			
			$user->name = $request->get('name');
			$user->email = $request->get('email');

			$role = Sentinel::findRoleById($request->get('roles'));
            if ($role) {
            	$user->roles()->detach();
                $role->users()->attach($user);
            }

			//update
			if (!Sentinel::update($user,$credenciais)) {
				throw new Exception("Não foi possivel atualizar os dados", 2);
			}

		} catch (Exception $e) {
			return redirect()->back()->withInput()->withErrors($e->getMessage());
		}

		return redirect()->route('dash.user.index');
	}


	public function updateMyAccount(UserAccountRequest $request,$id)
	{
		try {
			$user = Sentinel::getUser();
			$credenciais = [];
			//verificando se existe o campo password
			if ($request->has('password_old') && $request->has('password')) {
				$dados = [
				'email' => $user->email,
				'password' => $request->get('password_old')
				];
				//tentando autenticar o usuario com o email e password antigo
				if (!Sentinel::authenticate($dados,false))
				{
					throw new Exception("Password invalido!", 1);
					
				}
				//para trocar o password
				$credenciais['password'] = $request->get('password');
			}

			$user->name = $request->get('name');
			$user->email = $request->get('email');

			$role = Sentinel::findRoleById($request->get('roles'));
            if ($role) {
            	$user->roles()->detach();
                $role->users()->attach($user);
            }

			//update
			if (!Sentinel::update($user,$credenciais)) {
				throw new Exception("Não foi possivel atualizar os dados", 2);
			}

		} catch (Exception $e) {
			return redirect()->back()->withInput()->withErrors($e->getMessage());
		}

		return redirect()->route('dash.user.index');
	}
}
