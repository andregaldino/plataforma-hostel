<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleRequest;
use Sentinel;
use Exception;
use Toastr;

class RoleController extends Controller
{
    public function index()
    {
    	$roles = Sentinel::getRoleRepository()->paginate(10);
    	return View('admin.role.index',compact('roles'));
    }

    public function create()
    {
    	return View('admin.role.create');
    }

    public function edit($id=null)
    {
    	try {
    		$role = Sentinel::findRoleById($id);
            if ($role->permissions) {
                $permissions = json_decode(json_encode($role->permissions),true);
            }else{
                $permissions = [];
            }
            
    	} catch (Exception $e) {
            dd($e);
    		return redirect()->route('dash.role.index')->withErrors($e->getMessage());
    	}
    	return View('admin.role.edit',compact('role','permissions'));
    }

    public function store(RoleRequest $request)
    {
    	try {
    			if (Sentinel::findRoleBySlug(str_slug($request->input('name')))) {
    				throw new Exception("Permissão ja existente", 1);
    				
    			}
	            $role = Sentinel::getRoleRepository()->createModel()->create([
	                'name' => $request->input('name'),
	                'slug' => str_slug($request->input('name'))
	            ]);

                
                $permissions = array();

                foreach ($request->get('roles') as $key => $value) {
                    foreach ($value as $chave => $valor) {
                        $permissions[$key.".".$chave] = true;
                    }
                }

               

	            if (!$role) {
	            	throw new Exception("Não foi possivel criar a Permissão", 1);
	            }

                $role->permissions = $permissions;
                $role->save();
	    	return redirect()->route('dash.role.index');        
    	} catch (Exception $e) {
    		return redirect()->back()->withErrors($e->getMessage());
    	}
    	
    }

    public function update(RoleRequest $request, $id)
    {
    	try {
            $group = Sentinel::findRoleById($id);
	        $group->name = $request->get('name');
	         if (!$group->save()) {
	            throw new Exception("Não foi possivel atualizar a Permissão", 1);   
	        }
            $permissions = array();

            foreach ($request->get('roles') as $key => $value) {
                foreach ($value as $chave => $valor) {
                    $permissions[$key.".".$chave] = true;
                }
            }

            $group->permissions = $permissions;
            $group->save();

	        return redirect()->route('dash.role.index');
        } catch(Exception $e){
        	return redirect()->back()->withErrors($e->getMessage());
    	}
    }

    public function destroy($id = null)
    {
        try {
            $role = Sentinel::findRoleById($id);
            $role->delete();
            return redirect()->route('dash.role.index');
        } catch (Exception $e) {
        	return redirect()->back()->withErrors($e->getMessage());
        }        
    }

}
