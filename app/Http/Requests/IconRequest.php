<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IconRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
                break;
            case 'DELETE':
                break;
            case 'POST':
            {
                return [
                    'name'       => 'required|min:3',
                    'path' => 'required|mimes:svg',
                ];
                break;
            }
            case 'PUT':
                return [
                        'icon_id' => 'required|exists:icons,id',
                        'name'       => 'required|min:3',
                        'path' => 'mimes:svg',
                    ];
                break;
            case 'PATCH':
                break;
            default:
            break;
        }
    }
}
