<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
                break;
            case 'DELETE':
                break;
            case 'POST':
            {
                return [
                    'name'       => 'required|min:3',
                    'email'       => 'required|email|unique:users,email',
                    'password'       => 'required|min:6|confirmed',
                    'password_confirmation'       => 'required|min:6',
                    'roles'       => 'required|exists:roles,id',

                ];
                break;
            }
            case 'PUT':
                return [
                    'name'       => 'required|min:3',
                    'email'       => 'required|email|unique:users,email,'.$this->get('user_id'),
                    'password'       => 'min:6|confirmed',
                    'password_confirmation'       => 'min:6',
                    'roles'       => 'required|exists:roles,id',
                ];
                break;
            case 'PATCH':
                break;
            default:
            break;
        }
    }
}
