<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AccommodationRequest extends FormRequest
{
     /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
                break;
            case 'DELETE':
                break;
            case 'POST':
            {
                return [
                    'portuguese_title'       => 'required|min:3',
                    'portuguese_subtitle'       => 'required|min:3',
                    'portuguese_observations'       => 'required|min:3',
                    'portuguese_description'       => 'required|min:3',

                    'english_title'       => 'required|min:3',
                    'english_subtitle'       => 'required|min:3',
                    'english_observations'       => 'required|min:3',
                    'english_description'       => 'required|min:3',

                    'spanish_title'       => 'required|min:3',
                    'spanish_subtitle'       => 'required|min:3',
                    'spanish_observations'       => 'required|min:3',
                    'spanish_description'       => 'required|min:3',

                    'type_id' => 'required|exists:types,id',
                    'gallery_id' => 'exists:galleries,id',
                    'services.*'       => 'exists:services,id',

                    'beds' => 'required|numeric|min:1',
                    'gender' => 'required'

                ];
                break;
            }
            case 'PUT':
                return [
                        'portuguese_title'       => 'required|min:3',
                        'portuguese_subtitle'       => 'required|min:3',
                        'portuguese_observations'       => 'required|min:3',
                        'portuguese_description'       => 'required|min:3',
                        
                        'english_title'       => 'required|min:3',
                        'english_subtitle'       => 'required|min:3',
                        'english_observations'       => 'required|min:3',
                        'english_description'       => 'required|min:3',

                        'spanish_title'       => 'required|min:3',
                        'spanish_subtitle'       => 'required|min:3',
                        'spanish_observations'       => 'required|min:3',
                        'spanish_description'       => 'required|min:3',
                        
                        'type_id' => 'required|exists:types,id',
                        'gallery_id' => 'exists:galleries,id',
                        'services.*'       => 'exists:services,id',

                        'beds' => 'required|numeric|min:1',
                        'gender' => 'required'
                    ];
                break;
            case 'PATCH':
                break;
            default:
            break;
        }
    }
}
