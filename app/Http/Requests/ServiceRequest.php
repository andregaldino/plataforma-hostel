<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
                break;
            case 'DELETE':
                break;
            case 'POST':
            {
                return [
                    'portuguese_name'       => 'required|min:3',
                    'english_name'       => 'required|min:3',
                    'spanish_name'       => 'required|min:3',
                    'icon'       => 'required',
                ];
                break;
            }
            case 'PUT':
                return [
                    'portuguese_name'       => 'required|min:3',
                    'english_name'       => 'required|min:3',
                    'spanish_name'       => 'required|min:3',
                    'icon'       => 'required',
                ];
                break;
            case 'PATCH':
                break;
            default:
            break;
        }
    }
}
