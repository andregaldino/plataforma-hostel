<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use App\Traits\LangTrait;

class Term extends Model
{
    use LangTrait;
    
    protected $langOrderBy = 'term_id';
    protected $fillable = ['icon'];
    
    public function translations()
    {
        return $this->hasMany('App\TermTranslation');
    }

    // public function translation($language = null)
    // {
    //     if ($language == null) {
    //         $language = App::getLocale();
    //     }
    //     return $this->translations()->where('language', '=', $language)->orderBy('term_id','asc')->first();
    // }
}
