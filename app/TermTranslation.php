<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TermTranslation extends Model
{
    protected $table = 'term_translations';
    protected $fillable = ['language', 'title','description', 'term_id'];

    public function term()
    {
        return $this->belongsTo('App\Term');
    }
}
