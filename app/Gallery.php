<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gallery extends Model
{
	use SoftDeletes;
    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function photos()
    {
    	return $this->hasMany('App\Media');
    }

    public function event()
    {
        return $this->hasOne('App\Event')->withTrashed();
    }
}
