<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use App\Traits\LangTrait;

class Accommodation extends Model
{
    use SoftDeletes,Sluggable,SluggableScopeHelpers, LangTrait;
    protected $guarded = ['id'];
    protected $langOrderBy = 'accommodation_id';

    public function type()
    {
    	return $this->belongsTo('App\Type');
    }

    public function gallery()
    {
        return $this->belongsTo('App\Gallery')->withTrashed();
    }


     public function services()
    {
        return $this->belongsToMany('App\Service','accommodation_service');
    }


    // public function translation($language = null)
    // {
    //     if ($language == null) {
    //         $language = App::getLocale();
    //     }
    //     return $this->translations()->where('language', '=', $language)->orderBy('accommodation_id','asc')->first();
    // }

    public function translations()
    {
        return $this->hasMany('App\AccommodationTranslation');
    }



    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
