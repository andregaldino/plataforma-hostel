<?php 
namespace App\Traits;

use Illuminate\Support\Facades\App;

trait LangTrait{
	
	public function translation($language = null)
    {
        if ($language == null) {
            $language = App::getLocale();
        }

        $translation =  $this->translations()->where('language', '=', $language)->orderBy($this->langOrderBy,'asc')->first();
        if ($translation) {
        	return $translation;
        }
        return $this->translations()->orderBy($this->langOrderBy,'asc')->first();
    }
}