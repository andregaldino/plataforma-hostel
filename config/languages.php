<?php

return[
	'locales' => [
		'en'=>
			['language'=>'English','icon'=>'usa.svg'],
		'pt'=>
			['language'=>'Português','icon'=>'brazil.svg'],
		'es'=>
			['language'=>'Español','icon'=>'spain.svg'],
	]

];